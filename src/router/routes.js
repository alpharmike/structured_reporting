
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: "Home", component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/report',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'patientInfo', name: "PatientInfo", component: () => import('pages/PatientInfoPage.vue') },
      { path: 'patients', name: "Patients", component: () => import('pages/PatientsPage.vue') },
      { path: 'patientReports', name: "PatientReports", component: () => import('pages/PatientReportsPage.vue') },
      { path: 'report', name: "SingleReport", component: () => import('pages/SingleReportPage.vue') },
      { path: 'liver', name: "Liver", component: () => import('pages/LiverPage.vue') },
      { path: 'kidney', name: "Kidney", component: () => import('pages/KidneyPage.vue') },
      { path: 'prostate', name: "Prostate", component: () => import('pages/ProstatePage.vue') },
      { path: 'gallBladder', name: "GallBladder", component: () => import('pages/GallBladderPage.vue') },
      { path: 'spleen', name: "Spleen", component: () => import('pages/SpleenPage.vue') },
      { path: 'cbd', name: "CBD", component: () => import('pages/CBDPage.vue') },
      { path: 'pancreas', name: "Pancreas", component: () => import('pages/PancreasPage.vue') },
      { path: 'urinaryTract', name: "UrinaryTract", component: () => import('pages/UrinaryTractPage.vue') },
      { path: 'seminalVesicle', name: "SeminalVesicle", component: () => import('pages/SeminalVesiclePage.vue') },
      { path: 'vasculature', name: "Vasculature", component: () => import('pages/VasculaturePage.vue') },
      { path: 'abdoCavity', name: "AbdoCavity", component: () => import('pages/AbdoCavityPage.vue') },
      { path: 'uterus', name: "Uterus", component: () => import('pages/UterusPage.vue') },
      { path: 'ovaries', name: "Ovaries", component: () => import('pages/OvariesPage.vue') },
      { path: 'finalReport', name: "FinalReport", component: () => import('pages/FinalReport.vue') },
      { path: 'urinaryBladder', name: "UrinaryBladder", component: () => import('pages/UrinaryBladderPage.vue') },
      { path: 'ureter', name: "Ureter", component: () => import('pages/UreterPage.vue') },
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
