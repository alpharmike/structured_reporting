import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'Stone',
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'number',
          type: 'option',
          input: '',
          multiple: false,
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'one',
              value: 'one',
            },
            {
              id: uid(25),
              label: 'few',
              value: 'few',
            },
            {
              id: uid(25),
              label: 'many',
              value: 'many',
            },
          ]
        },
        {
          id: uid(25),
          label: 'size up to',
          type: 'text',
          input: '',
          colValue: '6'
        },
      ]
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'correlation with LFT or ERCP if clinically indicated',
      colValue: '6'
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'Dilation',
      colValue: '6',
      children: [
        {
          id: uid(25),
          type: 'checkbox',
          input: false,
          label: 'Generally',
          children: [
            {
              id: uid(25),
              label: 'max Size',
              type: 'text',
              input: ''
            }
          ]
        },
        {
          id: uid(25),
          type: 'checkbox',
          input: false,
          label: 'distal part',
          children: [
            {
              id: uid(25),
              label: 'max Size',
              type: 'text',
              input: ''
            }
          ]
        },
        {
          id: uid(25),
          type: 'checkbox',
          input: false,
          label: 'proxima part',
          children: [
            {
              id: uid(25),
              label: 'max size',
              type: 'text',
              input: ''
            }
          ]
        },
        {
          id: uid(25),
          type: 'checkbox',
          input: false,
          label: 'middle part',
          children: [
            {
              id: uid(25),
              label: 'max Size',
              type: 'text',
              input: ''
            }
          ]
        }
      ]
    }
  ],
  // inputFields: [
  //   {
  //     id: uid(25),
  //     label: 'evaluation status',
  //     type: 'option',
  //     multiple: false,
  //     input: 'evaluated',
  //     colValue: '12',
  //     options: [
  //       {
  //         id: uid(25),
  //         label: 'can not be evaluated',
  //         value: 'can not be evaluated',
  //       },
  //       {
  //         id: uid(25),
  //         label: 'evaluated',
  //         value: 'evaluated',
  //         children: [
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'Stone',
  //             colValue: '6',
  //             children: [
  //               {
  //                 id: uid(25),
  //                 label: 'number',
  //                 type: 'option',
  //                 input: '',
  //                 multiple: false,
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'one',
  //                     value: 'one',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'few',
  //                     value: 'few',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'many',
  //                     value: 'many',
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'size up to',
  //                 type: 'text',
  //                 input: '',
  //                 colValue: '6'
  //               },
  //             ]
  //           },
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'correlation with LFT or ERCP if clinically indicated',
  //             colValue: '6'
  //           },
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'Dilation',
  //             colValue: '6',
  //             children: [
  //               {
  //                 id: uid(25),
  //                 type: 'checkbox',
  //                 input: false,
  //                 label: 'Generally',
  //                 children: [
  //                   {
  //                     id: uid(25),
  //                     label: 'max Size',
  //                     type: 'text',
  //                     input: ''
  //                   }
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 type: 'checkbox',
  //                 input: false,
  //                 label: 'distal part',
  //                 children: [
  //                   {
  //                     id: uid(25),
  //                     label: 'max Size',
  //                     type: 'text',
  //                     input: ''
  //                   }
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 type: 'checkbox',
  //                 input: false,
  //                 label: 'proxima part',
  //                 children: [
  //                   {
  //                     id: uid(25),
  //                     label: 'max size',
  //                     type: 'text',
  //                     input: ''
  //                   }
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 type: 'checkbox',
  //                 input: false,
  //                 label: 'middle part',
  //                 children: [
  //                   {
  //                     id: uid(25),
  //                     label: 'max Size',
  //                     type: 'text',
  //                     input: ''
  //                   }
  //                 ]
  //               }
  //             ]
  //           }
  //         ]
  //       }
  //
  //
  //
  //     ]
  //   },
  // ]
}
const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value
    }
    state.inputFields = inputFields
  }
}
const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}
let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j
      < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }
    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i
      < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j
        < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }
    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly
    // The node has not been found and we have no more options
    return null
  }
}
const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
