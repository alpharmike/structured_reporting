import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'right ovaries',
      type: 'checkbox',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'size',
          type: 'option',
          input: 'normal for person\'s age',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal for person\'s age',
              value: 'normal for person\'s age'
            },
            {
              id: uid(25),
              label: 'small',
              value: 'small',
              children: [
                {
                  id: uid(25),
                  label: 'compatibility with pt\'s age',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'compatible with pt\'s age',
                      value: 'compatible with pt\'s age',
                    },
                    {
                      id: uid(25),
                      label: 'not compatible with pt\'s age',
                      value: 'not compatible with pt\'s age',
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size in MM',
                  type: 'text',
                  input: '',
                }
              ]
            },
            {
              id: uid(25),
              label: 'Increased size',
              value: 'Increased Size',
              children: [
                {
                  id: uid(25),
                  label: 'severity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'Mildly',
                      value: 'mildly'

                    },
                    {
                      id: uid(25),
                      label: 'Moderately',
                      value: 'moderately'
                    },
                    {
                      id: uid(25),
                      label: 'Severely',
                      value: 'severely'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'size',
                  type: 'text',
                  input: ''
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Shape/Position',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Abnormal',
              value: 'abnormal'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Echopattern',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Abnormal(heterogeneous)',
              value: 'abnormal'
            }
          ]
        },
        {
          id: uid(25),
          label: 'lesion(ovaries/adnexa)',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'number',
              type: 'option',
              multiple: false,
              input: '',
              options: [
                {
                  id: uid(25),
                  label: 'one',
                  value: 'one'
                },
                {
                  id: uid(25),
                  label: 'few',
                  value: 'few'
                },
                {
                  id: uid(25),
                  label: 'many',
                  value: 'many'
                },
              ]
            },
            {
              id: uid(25),
              label: 'size',
              type: 'text',
              input: '',
            },
            {
              id: uid(25),
              label: 'components',
              type: 'option',
              multiple: false,
              input: '',
              options: [
                {
                  id: uid(25),
                  label: 'follicles',
                  value: 'follicles'
                },
                {
                  id: uid(25),
                  label: 'cystic',
                  value: 'cystic'
                },
                {
                  id: uid(25),
                  label: 'solid',
                  value: 'solid'
                },
                {
                  id: uid(25),
                  label: 'mixture',
                  value: 'mixture'
                },
              ]
            },
            {
              id: uid(25),
              label: 'morphology',
              type: 'option',
              multiple: true,
              input: [],
              options: [
                {
                  id: uid(25),
                  label: 'spherical',
                  value: 'spherical'
                },
                {
                  id: uid(25),
                  label: 'oval',
                  value: 'oval'
                },
                {
                  id: uid(25),
                  label: 'tubular',
                  value: 'tubular'
                },
                {
                  id: uid(25),
                  label: 'follicular',
                  value: 'follicular'
                },
                {
                  id: uid(25),
                  label: 'loculated',
                  value: 'loculated'
                },
                {
                  id: uid(25),
                  label: 'haemorrhagic',
                  value: 'haemorrhagic'
                },
                {
                  id: uid(25),
                  label: 'encapsulated',
                  value: 'encapsulated',
                },
                {
                  id: uid(25),
                  label: 'septated',
                  value: 'septated',
                },
                {
                  id: uid(25),
                  label: 'internal calcification',
                  value: 'internal calcification',
                },
                {
                  id: uid(25),
                  label: 'fat component',
                  value: 'fat component',
                },
                {
                  id: uid(25),
                  label: 'central necrosis',
                  value: 'central necrosis',
                },
                {
                  id: uid(25),
                  label: 'internal vascularity',
                  value: 'internal vascularity',
                },
              ]
            },
            {
              id: uid(25),
              label: 'Echogenicity',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Hypoechoic',
                  value: 'hypoechoic'
                },

                {
                  id: uid(25),
                  label: 'Hyperechoic',
                  value: 'hyperechoic'
                },

                {
                  id: uid(25),
                  label: 'Isoechoic',
                  value: 'isoechoic'
                },
                {
                  id: uid(25),
                  label: 'Nonechoic',
                  value: 'nonechoic'
                }
              ],
            },
            {
              id: uid(25),
              label: 'Internal echo',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'homogeneous',
                  value: 'homogeneous'
                },
                {
                  id: uid(25),
                  label: 'heterogeneous',
                  value: 'heterogeneous'
                }
              ]
            },
            {
              id: uid(25),
              label: 'Border',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Regular',
                  value: 'regular'
                },
                {
                  id: uid(25),
                  label: 'Irregular',
                  value: 'irregular'
                }
              ]
            },
            {
              id: uid(25),
              type: 'checkbox',
              input: false,
              label: 'Sign of inflammation/collection around it',
            },
            {
              id: uid(25),
              label: 'Location',
              input: [],
              type: 'option',
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'right ovary',
                  value: 'right ovary'
                },
                {
                  id: uid(25),
                  label: 'fallopian tube',
                  value: 'fallopian tube',
                  children: [
                    {
                      id: uid(25),
                      label: 'location in fallopian tube',
                      input: [],
                      type: 'option',
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'infundibulum',
                          value: 'infundibulum'
                        },
                        {
                          id: uid(25),
                          label: 'ampulla',
                          value: 'ampulla'
                        },
                        {
                          id: uid(25),
                          label: 'isthmus',
                          value: 'isthmus'
                        },
                        {
                          id: uid(25),
                          label: 'intramural part',
                          value: 'intramural part'
                        }
                      ]
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'right ligaments',
                  value: 'right ligaments'
                },
                {
                  id: uid(25),
                  label: 'adjacent to right',
                  value: 'adjacent to right'
                }
              ]
            },
            {
              id: uid(25),
              label: 'suggestive of:',
              input: [],
              type: 'option',
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'benign lesion',
                  value: 'benign lesion'
                },
                {
                  id: uid(25),
                  label: 'malignant  lesion',
                  value: 'malignant  lesion'
                },
                {
                  id: uid(25),
                  label: 'follicular cyst',
                  value: 'follicular cyst'
                },
                {
                  id: uid(25),
                  label: 'corpus luteal cyst',
                  value: 'corpus luteal cyst'
                },
                {
                  id: uid(25),
                  label: 'polycystic ovaries PCOD',
                  value: 'polycystic ovaries PCOD'
                },
                {
                  id: uid(25),
                  label: 'endometrioma',
                  value: 'endometrioma'
                },

                {
                  id: uid(25),
                  label: 'tubo-ovarian abscess',
                  value: 'tubo-ovarian abscess'
                },
                {
                  id: uid(25),
                  label: 'PID',
                  value: 'PID'

                }
              ]
            }]
        },
        {
          id: uid(25),
          label: 'right adnexa',
          input: 'normal',
          type: 'option',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Abnormal',
              value: 'abnormal',
              children: [
                {
                  id: uid(25),
                  label: 'Sign Of:',
                  input: [],
                  type: 'option',
                  multiple: true,
                  options: [
                    {
                      id: uid(25),
                      label: 'signs of salpingitis',
                      value: 'signs of salpingitis'
                    },
                    {
                      id: uid(25),
                      label: 'signs of hydrosalpinx',
                      value: 'signs of hydrosalpinx'
                    },
                    {
                      id: uid(25),
                      label: 'signs of pyosalpinx',
                      value: 'signs of pyosalpinx'

                    }]
                },
              ]
            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'left ovaries',
      type: 'checkbox',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'size',
          type: 'option',
          input: 'normal for person\'s age',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal for person\'s age',
              value: 'normal for person\'s age'
            },
            {
              id: uid(25),
              label: 'small',
              value: 'small',
              children: [
                {
                  id: uid(25),
                  label: 'compatibility with pt\'s age',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'compatible with pt\'s age',
                      value: 'compatible with pt\'s age',
                    },
                    {
                      id: uid(25),
                      label: 'not compatible with pt\'s age',
                      value: 'not compatible with pt\'s age',
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size in MM',
                  type: 'text',
                  input: '',
                }
              ]
            },
            {
              id: uid(25),
              label: 'Increased size',
              value: 'Increased Size',
              children: [
                {
                  id: uid(25),
                  label: 'severity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'Mildly',
                      value: 'mildly'

                    },
                    {
                      id: uid(25),
                      label: 'Moderately',
                      value: 'moderately'
                    },
                    {
                      id: uid(25),
                      label: 'Severely',
                      value: 'severely'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'size',
                  type: 'text',
                  input: ''
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Shape/Position',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Abnormal',
              value: 'abnormal'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Echopattern',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Abnormal(heterogeneous)',
              value: 'abnormal'
            }
          ]
        },
        {
          id: uid(25),
          label: 'lesion(ovaries/adnexa)',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'number',
              type: 'option',
              multiple: false,
              input: '',
              options: [
                {
                  id: uid(25),
                  label: 'one',
                  value: 'one'
                },
                {
                  id: uid(25),
                  label: 'few',
                  value: 'few'
                },
                {
                  id: uid(25),
                  label: 'many',
                  value: 'many'
                },
              ]
            },
            {
              id: uid(25),
              label: 'size up to',
              type: 'text',
              input: '',
            },
            {
              id: uid(25),
              label: 'components',
              type: 'option',
              multiple: false,
              input: '',
              options: [
                {
                  id: uid(25),
                  label: 'follicles',
                  value: 'follicles'
                },
                {
                  id: uid(25),
                  label: 'cystic',
                  value: 'cystic'
                },
                {
                  id: uid(25),
                  label: 'solid',
                  value: 'solid'
                },
                {
                  id: uid(25),
                  label: 'mixture',
                  value: 'mixture'
                },
              ]
            },
            {
              id: uid(25),
              label: 'morphology',
              type: 'option',
              multiple: true,
              input: [],
              options: [
                {
                  id: uid(25),
                  label: 'spherical',
                  value: 'spherical'
                },
                {
                  id: uid(25),
                  label: 'oval',
                  value: 'oval'
                },
                {
                  id: uid(25),
                  label: 'tubular',
                  value: 'tubular'
                },
                {
                  id: uid(25),
                  label: 'follicular',
                  value: 'follicular'
                },
                {
                  id: uid(25),
                  label: 'loculated',
                  value: 'loculated'
                },
                {
                  id: uid(25),
                  label: 'haemorrhagic',
                  value: 'haemorrhagic'
                },
                {
                  id: uid(25),
                  label: 'encapsulated',
                  value: 'encapsulated',
                },
                {
                  id: uid(25),
                  label: 'septated',
                  value: 'septated',
                },
                {
                  id: uid(25),
                  label: 'internal calcification',
                  value: 'internal calcification',
                },
                {
                  id: uid(25),
                  label: 'fat component',
                  value: 'fat component',
                },
                {
                  id: uid(25),
                  label: 'central necrosis',
                  value: 'central necrosis',
                },
                {
                  id: uid(25),
                  label: 'internal vascularity',
                  value: 'internal vascularity',
                },
              ]
            },
            {
              id: uid(25),
              label: 'Echogenicity',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Hypoechoic',
                  value: 'hypoechoic'
                },

                {
                  id: uid(25),
                  label: 'Hyperechoic',
                  value: 'hyperechoic'
                },

                {
                  id: uid(25),
                  label: 'Isoechoic',
                  value: 'isoechoic'
                },
                {
                  id: uid(25),
                  label: 'Nonechoic',
                  value: 'nonechoic'
                }
              ],
            },
            {
              id: uid(25),
              label: 'Internal echo',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'homogeneous',
                  value: 'homogeneous'
                },
                {
                  id: uid(25),
                  label: 'heterogeneous',
                  value: 'heterogeneous'
                }
              ]
            },
            {
              id: uid(25),
              label: 'Border',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Regular',
                  value: 'regular'
                },
                {
                  id: uid(25),
                  label: 'Irregular',
                  value: 'irregular'
                }
              ]
            },
            {
              id: uid(25),
              type: 'checkbox',
              input: false,
              label: 'Sign of inflammation/collection around it',
            },
            {
              id: uid(25),
              label: 'Location',
              input: [],
              type: 'option',
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'left ovary',
                  value: 'left ovary'
                },
                {
                  id: uid(25),
                  label: 'fallopian tube',
                  value: 'fallopian tube',
                  children: [
                    {
                      id: uid(25),
                      label: 'location in fallopian tube',
                      input: [],
                      type: 'option',
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'infundibulum',
                          value: 'infundibulum'
                        },
                        {
                          id: uid(25),
                          label: 'ampulla',
                          value: 'ampulla'
                        },
                        {
                          id: uid(25),
                          label: 'isthmus',
                          value: 'isthmus'
                        },
                        {
                          id: uid(25),
                          label: 'intramural part',
                          value: 'intramural part'
                        }
                      ]
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'left ligaments',
                  value: 'left ligaments'
                },
                {
                  id: uid(25),
                  label: 'adjacent to left',
                  value: 'adjacent to left'
                }
              ]
            },
            {
              id: uid(25),
              label: 'suggestive of:',
              input: [],
              type: 'option',
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'benign lesion',
                  value: 'benign lesion'
                },
                {
                  id: uid(25),
                  label: 'malignant  lesion',
                  value: 'malignant  lesion'
                },
                {
                  id: uid(25),
                  label: 'follicular cyst',
                  value: 'follicular cyst'
                },
                {
                  id: uid(25),
                  label: 'corpus luteal cyst',
                  value: 'corpus luteal cyst'
                },
                {
                  id: uid(25),
                  label: 'polycystic ovaries PCOD',
                  value: 'polycystic ovaries PCOD'
                },
                {
                  id: uid(25),
                  label: 'endometrioma',
                  value: 'endometrioma'
                },

                {
                  id: uid(25),
                  label: 'tubo-ovarian abscess',
                  value: 'tubo-ovarian abscess'
                },
                {
                  id: uid(25),
                  label: 'PID',
                  value: 'PID'

                }
              ]
            }]
        },
        {
          id: uid(25),
          label: 'left adnexa',
          input: 'normal',
          type: 'option',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Abnormal',
              value: 'abnormal',
              children: [
                {
                  id: uid(25),
                  label: 'Sign Of:',
                  input: [],
                  type: 'option',
                  multiple: true,
                  options: [
                    {
                      id: uid(25),
                      label: 'signs of salpingitis',
                      value: 'signs of salpingitis'
                    },
                    {
                      id: uid(25),
                      label: 'signs of hydrosalpinx',
                      value: 'signs of hydrosalpinx'
                    },
                    {
                      id: uid(25),
                      label: 'signs of pyosalpinx',
                      value: 'signs of pyosalpinx'
                    }]
                },
              ]
            }
          ]
        }
      ]
    }
  ]
}

const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value

    }
    state.inputFields = inputFields
  }
}

const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly

    // The node has not been found and we have no more options
    return null
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
