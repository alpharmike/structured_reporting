import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'Size',
      type: 'option',
      input: 'normal',
      multiple: false,
      options: [
        {
          id: uid(25),
          label: 'Normal',
          value: 'normal',
          children: [
            {
              id: uid(25),
              label: 'Size in MM',
              type: 'text',
              input: ''
            }
          ]
        },
        {
          id: uid(25),
          label: 'Small',
          value: 'small',
          children: [
            {
              id: uid(25),
              label: 'Size in MM',
              type: 'text',
              input: ''
            },
            {
              id: uid(25),
              label: 'Reason',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Compatible with Patient\'s Age',
                  value: 'compatible'
                },
                {
                  id: uid(25),
                  label: 'Due to Post-Menopausal State',
                  value: 'due'
                },
                {
                  id: uid(25),
                  label: 'Non-Compatible with Patient\'s Age and Condition',
                  value: 'non-compatible'
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Enlarged',
          value: 'enlarged',
          children: [
            {
              id: uid(25),
              label: 'Size in MM',
              type: 'text',
              input: ''
            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'Position',
      type: 'option',
      input: 'normal',
      multiple: false,
      options: [
        {
          id: uid(25),
          label: 'Normal',
          value: 'normal'
        },
        {
          id: uid(25),
          label: 'abnormal',
          value: 'abnormal',
          children: [
            {
              id: uid(25),
              label: 'abnormality type',
              type: 'option',
              input: 'Anteverted',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Anteverted',
                  value: 'anteverted'
                },
                {
                  id: uid(25),
                  label: 'Retroverted',
                  value: 'retroverted'
                },
                {
                  id: uid(25),
                  label: 'Anteflexed',
                  value: 'anteflexed'
                },
                {
                  id: uid(25),
                  label: 'Retroflexed',
                  value: 'retroflexed'
                }
              ]
            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'endometrium',
      type: 'option',
      input: 'normal',
      multiple: false,
      options: [
        {
          id: uid(25),
          label: 'intact',
          value: 'intact',
          children: [
            {
              id: uid(25),
              label: 'endometrial thickness',
              type: 'text',
              input: '',
            },
            {
              id: uid(25),
              label: "sign of abnormal fluid collection or bleeding in endometrial cavity(hematometra/clot",
              type: 'option',
              input: [],
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'positive',
                  value: 'positive'
                },
                {
                  id: uid(25),
                  label: 'negative',
                  value: 'negative'
                },
              ]

            }
          ]
        },
        {
          id: uid(25),
          label: 'not intact',
          value: 'not intact',
          children: [
            {
              id: uid(25),
              label: 'endometrial thickness',
              type: 'text',
              input: '',
            },
            {
              id: uid(25),
              label: "sign of abnormal fluid collection or bleeding in endometrial cavity(hematometra/clot",
              type: 'option',
              input: [],
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'positive',
                  value: 'positive'
                },
                {
                  id: uid(25),
                  label: 'negative',
                  value: 'negative'
                },
              ]

            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'Shape',
      type: 'option',
      input: 'normal',
      multiple: false,
      options: [
        {
          id: uid(25),
          label: 'Normal',
          value: 'normal'
        },
        {
          id: uid(25),
          label: 'Abnormal',
          value: 'abnormal',
          children: [
            {
              id: uid(25),
              label: 'Abnormal Type',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Uterus Didelphys',
                  value: 'uterus-didelphys'
                },
                {
                  id: uid(25),
                  label: 'Unicornuate Uterus',
                  value: 'unicornuate-uterus'
                },
                {
                  id: uid(25),
                  label: 'Bicornuate Uterus',
                  value: 'bicornuate-uterus'
                },
                {
                  id: uid(25),
                  label: 'Septate Uterus',
                  value: 'septate-uterus'
                },
                {
                  id: uid(25),
                  label: 'Arcuate Uterus',
                  value: 'arcuate-uterus'
                }
              ]
            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'Myometrial Parenchymal Echogenicity',
      type: 'option',
      input: 'normal',
      multiple: false,
      options: [
        {
          id: uid(25),
          label: 'Normal(homogeneous)',
          value: 'normal'
        },
        {
          id: uid(25),
          label: 'Heterogenous',
          value: 'heterogenous'
        }
      ]
    },
    {
      id: uid(25),
      label: 'Sign of Mass Lesion',
      type: 'checkbox',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'Number',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'One',
              value: 'one'
            },
            {
              id: uid(25),
              label: 'Few',
              value: 'few'
            },
            {
              id: uid(25),
              label: 'Many',
              value: 'many'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Components',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'cystic',
              value: 'cystic'
            },
            {
              id: uid(25),
              label: 'Solid',
              value: 'solid'
            },
            {
              id: uid(25),
              label: 'Mixture',
              value: 'mixture'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Morphology',
          type: 'option',
          input: [],
          multiple: true,
          options: [
            {
              id: uid(25),
              label: 'Spherical',
              value: 'spherical'
            },
            {
              id: uid(25),
              label: 'Ovoid',
              value: 'ovoid'
            },
            {
              id: uid(25),
              label: 'Tubular',
              value: 'tubular'
            },
            {
              id: uid(25),
              label: 'Lobular',
              value: 'lobular'
            },
            {
              id: uid(25),
              label: 'PolyPoid',
              value: 'polypoid',
              children: [
                {
                  id: uid(25),
                  label: 'Polypoid Type',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'Pedunculated',
                      value: 'pedunculated'
                    },
                    {
                      id: uid(25),
                      label: 'Sessile',
                      value: 'sessile'
                    }
                  ]
                }
              ]
            },
            {
              id: uid(25),
              label: 'Loculated',
              value: 'loculated'
            },
            {
              id: uid(25),
              label: 'Internal Septation',
              value: 'internal-septation'
            },
            {
              id: uid(25),
              label: 'Internal Hemorrhage',
              value: 'internal-hemorrhage'
            },
            {
              id: uid(25),
              label: 'Internal Calcification',
              value: 'internal-calcification'
            },
            {
              id: uid(25),
              label: 'Sonographic Shadowing',
              value: 'sonographic-shadowing'
            },
            {
              id: uid(25),
              label: 'Central Necrosis',
              value: 'central-necrosis'
            },
            {
              id: uid(25),
              label: 'Internal Vascularity',
              value: 'internal-vascularity'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Echogenicity',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Hyperecho',
              value: 'hyperecho'
            },
            {
              id: uid(25),
              label: 'Isoecho',
              value: 'isoecho'
            },
            {
              id: uid(25),
              label: 'Hypoecho',
              value: 'hypoecho'
            },
            {
              id: uid(25),
              label: 'Non Echoic',
              value: 'non-echoic'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Internal Echo',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Homogeneous',
              value: 'homogeneous'
            },
            {
              id: uid(25),
              label: 'Heterogeneous',
              value: 'heterogeneous'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Border',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Regular',
              value: 'regular'
            },
            {
              id: uid(25),
              label: 'Irregular',
              value: 'irregular'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Invasion to Surrounding tissue',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'Sign of Inflammation/Collection around it',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'Location',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Cervical Canal',
              value: 'cervical-canal'
            },
            {
              id: uid(25),
              label: 'Uterus',
              value: 'uterus',
              children: [
                {
                  id: uid(25),
                  label: 'Anatomic',
                  type: 'checkbox',
                  input: false,
                  children: [
                    {
                      id: uid(25),
                      label: 'Anatomic Options',
                      type: 'option',
                      input: [],
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'Fundus',
                          value: 'fundus',
                          children: [
                            {
                              id: uid(25),
                              label: 'fundus status',
                              type: 'option',
                              input: '',
                              multiple: false,
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Ant Wall',
                                  value: 'ant-wall'
                                },
                                {
                                  id: uid(25),
                                  label: 'Pos Wall',
                                  value: 'pos-wall'
                                }
                              ]
                            }
                          ]
                        },
                        {
                          id: uid(25),
                          label: 'Body',
                          value: 'body',
                          children: [
                            {
                              id: uid(25),
                              label: 'body status',
                              multiple: false,
                              type: 'option',
                              input: '',
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Ant Wall',
                                  value: 'ant-wall'
                                },
                                {
                                  id: uid(25),
                                  label: 'Pos Wall',
                                  value: 'pos-wall'
                                }
                              ]
                            }
                          ]
                        },
                        {
                          id: uid(25),
                          label: 'Isthmus',
                          value: 'isthmus',
                          children: [
                            {
                              id: uid(25),
                              label: 'isthmus status',
                              multiple: false,
                              type: 'option',
                              input: '',
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Ant Wall',
                                  value: 'ant-wall'
                                },
                                {
                                  id: uid(25),
                                  label: 'Pos Wall',
                                  value: 'pos-wall'
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]

                },
                {
                  id: uid(25),
                  label: 'Histologic',
                  type: 'checkbox',
                  input: false,
                  children: [
                    {
                      id: uid(25),
                      label: 'Histologic Options',
                      multiple: false,
                      type: 'option',
                      input: '',
                      options: [
                        {
                          id: uid(25),
                          label: 'Submucosal',
                          value: 'submucosal',
                          children: [
                            {
                              id: uid(25),
                              label: 'Submucosal Options',
                              multiple: false,
                              type: 'option',
                              input: '',
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Completely within Endometrial Cavity',
                                  value: 'completely-within-endometrial-cavity'
                                },
                                {
                                  id: uid(25),
                                  label: 'Extend into the Myometrium',
                                  value: 'extend-into-the-myometrium'
                                }
                              ]
                            }
                          ]
                        },
                        {
                          id: uid(25),
                          label: 'Subserosal',
                          value: 'subserosal'
                        },
                        {
                          id: uid(25),
                          label: 'Intramural',
                          value: 'intramural'
                        }
                      ]
                    }
                  ]
                }

              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Size',
          multiple: false,
          type: 'option',
          input: 'normal',
          options: [
            {
              id: uid(25),
              label: 'Normal',
              value: 'normal',
              children: [
                {
                  id: uid(25),
                  label: 'Size in MM',
                  type: 'text',
                  input: ''
                }
              ]
            },
            {
              id: uid(25),
              label: 'Small',
              value: 'small',
              children: [
                {
                  id: uid(25),
                  label: 'Size in MM',
                  type: 'text',
                  input: ''
                }
              ]
            },
            {
              id: uid(25),
              label: 'Enlarged',
              value: 'enlarged',
              children: [
                {
                  id: uid(25),
                  label: 'Size in MM',
                  type: 'text',
                  input: ''
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Suggestive Of',
          type: 'option',
          input: [],
          multiple: true,
          options: [
            {
              id: uid(25),
              label: 'Being Lesion',
              value: 'being-lesion'
            },
            {
              id: uid(25),
              label: 'Malignant Lesion',
              value: 'malignant-lesion',
              children: [
                {
                  id: uid(25),
                  label: 'Carcinoma',
                  type: 'checkbox',
                  input: false
                }
              ]
            },
            {
              id: uid(25),
              label: 'Adenomyosis',
              value: 'adenomyosis'
            },
            {
              id: uid(25),
              label: 'Leiomyoma(fibroid)',
              value: 'leiomyoma'
            },
            {
              id: uid(25),
              label: 'Nabothian Cervical Cyst',
              value: 'nabothian-cervical-cyst'
            }
          ]
        }
      ]
    }
  ]
}

const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    const inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      const res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value
    }
    state.inputFields = inputFields
  }
}

const actions = {
  updateVasculatureValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

const recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }
    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise 'return' will not work properly

    // The node has not been found and we have no more options
    return null
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}
const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
