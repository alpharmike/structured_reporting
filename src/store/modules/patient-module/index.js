import Vue from 'vue'
import uid from 'uid'
import axios from 'axios';
import urls from "src/ServerConfig/ServerConfig";

const state = {
  patientInfo: {
    firstName: "",
    lastName: "",
    phoneNumber: "",
    nationalCode: "",
    age: "",
    gender: "male",
  },

  patients: [],
  patientId: null,
  currentPatientGender: null,
  patientReports: [],
  currentReport: null
};
const mutations = {
  updateFieldValue (state, payload) {
    state.patientInfo[payload.field] = payload.value;
  },

  setPatients (state, payload) {
    state.patients = payload;
  },

  savePatientInfo(state, payload) {
    state.patientId = payload.id;
    state.currentPatientGender = payload.gender;
  },

  saveReport(state, payload) {
    state.currentReport = payload;
  },

  setPatientReports(state, payload) {
    state.patientReports = payload;
  },

  cleanReports(state, payload) {
    state.patientReports = [];
  },

  setReports(state, payload) {
    state.patientReports = payload;

  }
};
const actions = {
  updateValue(context, info) {
    context.commit("updateFieldValue", info)
  },

  getAllPatients(context) {
    return axios.get(urls.GetAllPatients).then((response) => {
      context.commit('setPatients', response.data);
    });
  },

  postPatientInfo(context, info) {
    return axios.post(urls.CreatePatient, context.state.patientInfo);
  },

  setPatientInfo(context, payload) {
    if(context.state.patientId !== payload.id) {
      context.commit('savePatientInfo', payload);
      context.commit('setPatientReports', []);
    }
  },

  postPatientReport(context, report) {
    return axios.post(
      urls.CreateReport,
      {
        id: context.state.patientId,
        report: report
      }
    );
  },

  getPatientReports(context, patientId) {
    if (patientId === context.state.patientId) {
      context.commit('cleanReports', []);
      return axios.get(
        urls.GetPatientReports + '/' + context.state.patientId
      ).then((response) => {
        context.commit('setReports', response.data);
        response.data.forEach((reportId) => {

        })

      })
    }
  },

  getSingleReport(context, reportId) {
    return axios.get(
      urls.GetReport +'/' + reportId
    ).then((response) => {
      context.commit('saveReport', response.data.reportBody);
    })
  },

  // setPatientCurrentReport(context, reportId) {
  //   let reportIndex = context.state.patientReports.findIndex(currReport => currReport['_id'] === reportId);
  //   let report;
  //   if (reportIndex !== -1) {
  //     if (report !== context.state.patientReports[reportIndex]) {
  //       report = context.state.patientReports[reportIndex]
  //     }
  //   }
  //   console.log(report)
  //   context.commit('saveCurrentReport', report);
  //
  // }

};

const getters = {
  patientInfo: (state) => {
    return state.patientInfo
  },

  patients: (state) => {
    return state.patients
  },

  patientId: (state) => {
    return state.patientId
  },

  currentPatientGender: (state) => {
    return state.currentPatientGender
  },

  patientReports: (state) => {
    return state.patientReports
  },

  currentReport: (state) => {
    return state.currentReport
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
