import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'Status',
      type: 'option',
      input: 'normal',
      multiple: false,
      colValue: '12',
      options: [
        {
          id: uid(25),
          label: 'Normal',
          value: 'normal',
        },
        {
          id: uid(25),
          label: 'Abnormal',
          value: 'abnormal',
          children: [
            {
              id: uid(25),
              label: 'Side',
              type: 'option',
              multiple: false,
              input: '',
              colValue: '6',
              options: [
                {
                  id: uid(25),
                  label: 'Right',
                  value: 'right'
                },
                {
                  id: uid(25),
                  label: 'Left',
                  value: 'left'
                },
                {
                  id: uid(25),
                  label: 'Both',
                  value: 'both'
                }
              ]
            },
            {
              id: uid(25),
              label: 'size',
              type: 'option',
              multiple: false,
              input: 'normal',
              colValue: '6',
              options: [
                {
                  id: uid(25),
                  label: 'normal',
                  value: 'normal'
                },
                {
                  id: uid(25),
                  label: 'small/hypoplasia',
                  value: 'small/hypoplasia'
                },
                {
                  id: uid(25),
                  label: 'enlarged',
                  value: 'enlarged'
                },
              ]
            },
            {
              id: uid(25),
              type: 'checkbox',
              input: false,
              label: 'Abnormal shape',
              colValue: '6',
            },
            {
              id: uid(25),
              type: 'checkbox',
              input: false,
              label: 'Abnormal echotexture',
              colValue: '6',
            },
            {
              id: uid(25),
              label: 'lesion',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'Number of seen lesions',
                  type: 'option',
                  input: '',
                  multiple: false,
                  colValue: '6',
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one',
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few',
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many',
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'Echogenicity',
                  type: 'option',
                  multiple: false,
                  input: '',
                  colValue: '6',
                  options: [
                    {
                      id: uid(25),
                      label: 'Hypoechoic',
                      value: 'hypoechoic'
                    },
                    {
                      id: uid(25),
                      label: 'Hyperechoic',
                      value: 'hyperechoic'
                    },
                    {
                      id: uid(25),
                      label: 'Isoechoic',
                      value: 'isoechoic'
                    },
                    {
                      id: uid(25),
                      label: 'Nonechoic',
                      value: 'nonechoic'
                    }
                  ],
                },
                {
                  id: uid(25),
                  label: 'components',
                  type: 'option',
                  multiple: false,
                  input: '',
                  colValue: '6',
                  options: [
                    {
                      id: uid(25),
                      label: 'cyst',
                      value: 'cyst'
                    },
                    {
                      id: uid(25),
                      label: 'solid',
                      value: 'solid'
                    },
                    {
                      id: uid(25),
                      label: 'mixture',
                      value: 'mixture'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'morphology',
                  type: 'option',
                  multiple: true,
                  input: [],
                  colValue: '6',
                  options: [
                    {
                      id: uid(25),
                      label: 'tubular',
                      value: 'tubular'
                    },
                    {
                      id: uid(25),
                      label: 'lobular',
                      value: 'lobular'
                    },
                    {
                      id: uid(25),
                      label: 'Encapsulated',
                      value: 'Encapsulated'
                    },
                    {
                      id: uid(25),
                      label: 'loculated',
                      value: 'loculated'
                    },
                    {
                      id: uid(25),
                      label: 'encapsulated',
                      value: 'encapsulated'
                    },
                    {
                      id: uid(25),
                      label: 'internal septation',
                      value: 'internal septation'
                    },
                    {
                      id: uid(25),
                      label: 'hemorrhagic',
                      value: 'hemorrhagic'
                    },
                    {
                      id: uid(25),
                      label: 'calcification',
                      value: 'calcification'
                    },
                    {
                      id: uid(25),
                      label: 'internal vascularity',
                      value: 'internal vascularity'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'Internal echo',
                  type: 'option',
                  multiple: false,
                  input: '',
                  colValue: '6',
                  options: [
                    {
                      label: 'homogenous',
                      value: 'homogenous'
                    },
                    {
                      label: 'heterogenous',
                      value: 'heterogenous'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'Border',
                  type: 'option',
                  multiple: false,
                  input: '',
                  colValue: '6',
                  options: [
                    {
                      label: 'Regular',
                      value: 'regular'
                    },
                    {
                      label: 'Irregular',
                      value: 'irregular'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'Size of lesion Up To',
                  input: '',
                  type: 'text',
                },
                {
                  id: uid(25),
                  type: 'checkbox',
                  input: false,
                  label: 'invasion to surrounding tissue',
                  colValue: '6'
                },
                {
                  id: uid(25),
                  type: 'checkbox',
                  input: false,
                  label: 'sign of inflammation/collection around it',
                  colValue: '6'
                },
                {
                  id: uid(25),
                  label: 'suggestive of:',
                  type: 'option',
                  multiple: true,
                  input: [],
                  options: [
                    {
                      id: uid(25),
                      label: 'benign lesion',
                      value: 'benign lesion'
                    },
                    {
                      id: uid(25),
                      label: 'malignant  lesion',
                      value: 'malignant  lesion'
                    },
                  ]
                }]
            },
          ]
        },
        {
          id: uid(25),
          label: 'not seen',
          value: 'not seen'
        }
      ]
    },
  ]
}
const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value
    }
    state.inputFields = inputFields
  }
}
const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}
let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j
      < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }
    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i
      < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j
        < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }
    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly
    // The node has not been found and we have no more options
    return null
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
