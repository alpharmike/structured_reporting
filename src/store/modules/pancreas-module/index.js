import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'size',
      type: 'option',
      multiple: false,
      input: 'normal',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text',
            colValue: '12',
          }]
        },
        {
          id: uid(),
          label: 'small',
          value: 'small',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text',
            colValue: '12',
          }]
        },
        {
          id: uid(25),
          label: 'prominent(enlarged)',
          value: 'prominent',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text',
            colValue: '12',
          }],
        }
      ]
    },
    {
      id: uid(25),
      label: 'shape',
      type: 'option',
      multiple: false,
      input: 'normal',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal',
        },
        {
          id: uid(25),
          label: 'abnormal',
          value: 'abnormal',
          children: [
            {
              id: uid(25),
              label: 'abnormal reporte',
              type: 'text'
            }
          ]
        }
      ]
    },

    {
      id: uid(25),
      label: 'Parenchymal Echogenicity',
      type: 'option',
      multiple: false,
      input: 'Normal(homogeneous)',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'Normal(homogeneous)',
          value: 'normal(homogeneous)'
        },
        {
          id: uid(25),
          label: 'heterogeneous',
          value: 'heterogeneous'
        },
      ]
    },

    {
      id: uid(25),
      label: 'pancreatic duct',
      type: 'option',
      multiple: false,
      input: 'normal',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal',
        },
        {
          id: uid(25),
          label: 'dilated',
          value: 'dilated',
          children: [
            {
              id: uid(25),
              label: 'size',
              type: 'text',
              input: '',
            }
          ]
        },
      ]
    },

    {
      id: uid(25),
      label: 'Sign of Space Occupying Lesion',
      type: 'checkbox',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'number',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'one',
              value: 'one'
            },
            {
              id: uid(25),
              label: 'few',
              value: 'few'
            },
            {
              id: uid(25),
              label: 'many',
              value: 'many'
            },
          ]
        },
        {
          id: uid(25),
          label: 'echogenicity',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'hyperechoic',
              value: 'hyperechoic',
            },
            {
              id: uid(25),
              label: 'isoechoic',
              value: 'isoechoic',
            },
            {
              id: uid(25),
              label: 'hypoechoic',
              value: 'hypoechoic',
            },
            {
              id: uid(25),
              label: 'nonechoic',
              value: 'nonechoic',
            }
          ]
        },
        {
          id: uid(25),
          label: 'components',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'cystic',
              value: 'cystic'
            },
            {
              id: uid(25),
              label: 'solid',
              value: 'solid'
            },
            {
              id: uid(25),
              label: 'mixture',
              value: 'mixture'
            },
          ]
        },
        {
          id: uid(25),
          label: 'morphology',
          type: 'option',
          multiple: true,
          input: [],
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'spherical',
              value: 'spherical'
            },
            {
              id: uid(25),
              label: 'oval',
              value: 'oval'
            },
            {
              id: uid(25),
              label: 'lobular',
              value: 'lobular'
            },
            {
              id: uid(25),
              label: 'tubular',
              value: 'tubular'
            },
            {
              id: uid(25),
              label: 'loculated',
              value: 'loculated'
            },
            {
              id: uid(25),
              label: 'encapsulated',
              value: 'encapsulated',
            },
            {
              id: uid(25),
              label: 'septated',
              value: 'septated',
            },
            {
              id: uid(25),
              label: 'internal hemorrhage',
              value: 'internal hemorrhage',
            },
            {
              id: uid(25),
              label: 'internal calcification',
              value: 'internal calcification',
            },
            {
              id: uid(25),
              label: 'central necrosis',
              value: 'central necrosis',
            },
            {
              id: uid(25),
              label: 'internal vascularity',
              value: 'internal vascularity',
            },
          ]
        },
        {
          id: uid(25),
          label: 'border',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'regular',
              value: 'regular',
            },
            {
              id: uid(25),
              label: 'irregular',
              value: 'irregular',
            },
          ]
        },
        {
          id: uid(25),
          label: 'location',
          type: 'option',
          multiple: true,
          input: [],
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'head',
              value: 'head',
            },
            {
              id: uid(25),
              label: 'neck',
              value: 'neck',
            },
            {
              id: uid(25),
              label: 'body',
              value: 'body',
            },
            {
              id: uid(25),
              label: 'tail',
              value: 'tail',
            },
            {
              id: uid(25),
              label: 'uncinate process',
              value: 'uncinate process',
            },
          ]
        },
        {
          id: uid(25),
          label: 'suggestive of',
          type: 'option',
          multiple: true,
          input: [],
          options: [
            {
              id: uid(25),
              label: 'cyst/pseudocyst',
              value: 'cyst/pseudocyst',
            },
            {
              id: uid(25),
              label: 'abscess',
              value: 'abscess',
            },
            {
              id: uid(25),
              label: 'malignant lesion/metastasis',
              value: 'malignant',
            },
            {
              id: uid(25),
              label: 'being lesion',
              value: 'being lesion',
            },
          ]
        },
        {
          id: uid(25),
          label: 'invasion to surrounding tissue',
          type: 'checkbox',
          input: false,
          colValue: '6'
        },
        {
          id: uid(25),
          label: 'sign of inflammation/collection around it',
          type: 'checkbox',
          input: false,
          colValue: '6'
        },
      ]
    },
    {
      id: uid(25),
      label: 'Sign of Pancreatitis',
      type: 'checkbox',
      input: false,
      colValue: '6'
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'correlation with CT san is recommended',
    },
  ],
  // inputFields: [
  //   {
  //     id: uid(25),
  //     label: 'Evaluation Status',
  //     type: 'option',
  //     multiple: false,
  //     input: 'evaluated',
  //     colValue: '12',
  //     options: [
  //       {
  //         id: uid(25),
  //         label: 'could not be evaluated completely due to severe gas shadow',
  //         value: 'could not be evaluated completely due to severe gas shadow',
  //       },
  //       {
  //         id: uid(25),
  //         label: 'evaluated',
  //         value: 'evaluated',
  //         children: [
  //           {
  //             id: uid(25),
  //             label: 'size',
  //             type: 'option',
  //             multiple: false,
  //             input: 'normal',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'normal',
  //                 value: 'normal',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Measured Size',
  //                   input: '',
  //                   type: 'text',
  //                   colValue: '12',
  //                 }]
  //               },
  //               {
  //                 id: uid(),
  //                 label: 'small',
  //                 value: 'small',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Measured Size',
  //                   input: '',
  //                   type: 'text',
  //                   colValue: '12',
  //                 }]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'prominent(enlarged)',
  //                 value: 'prominent',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Measured Size',
  //                   input: '',
  //                   type: 'text',
  //                   colValue: '12',
  //                 }],
  //               }
  //             ]
  //           },
  //           {
  //             id: uid(25),
  //             label: 'shape',
  //             type: 'option',
  //             multiple: false,
  //             input: 'normal',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'normal',
  //                 value: 'normal',
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'abnormal',
  //                 value: 'abnormal',
  //                 children: [
  //                   {
  //                     id: uid(25),
  //                     label: 'abnormal reporte',
  //                     type: 'text'
  //                   }
  //                 ]
  //               }
  //             ]
  //           },
  //
  //           {
  //             id: uid(25),
  //             label: 'Parenchymal Echogenicity',
  //             type: 'option',
  //             multiple: false,
  //             input: 'Normal(homogeneous)',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'Normal(homogeneous)',
  //                 value: 'normal(homogeneous)'
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'heterogeneous',
  //                 value: 'heterogeneous'
  //               },
  //             ]
  //           },
  //
  //           {
  //             id: uid(25),
  //             label: 'pancreatic duct',
  //             type: 'option',
  //             multiple: false,
  //             input: 'normal',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'normal',
  //                 value: 'normal',
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'dilated',
  //                 value: 'dilated',
  //                 children: [
  //                   {
  //                     id: uid(25),
  //                     label: 'size',
  //                     type: 'text',
  //                     input: '',
  //                   }
  //                 ]
  //               },
  //             ]
  //           },
  //
  //           {
  //             id: uid(25),
  //             label: 'Sign of Space Occupying Lesion',
  //             type: 'checkbox',
  //             input: false,
  //             children: [
  //               {
  //                 id: uid(25),
  //                 label: 'number',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'one',
  //                     value: 'one'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'few',
  //                     value: 'few'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'many',
  //                     value: 'many'
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'echogenicity',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'hyperechoic',
  //                     value: 'hyperechoic',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'isoechoic',
  //                     value: 'isoechoic',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'hypoechoic',
  //                     value: 'hypoechoic',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'nonechoic',
  //                     value: 'nonechoic',
  //                   }
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'components',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'cystic',
  //                     value: 'cystic'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'solid',
  //                     value: 'solid'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'mixture',
  //                     value: 'mixture'
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'morphology',
  //                 type: 'option',
  //                 multiple: true,
  //                 input: [],
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'spherical',
  //                     value: 'spherical'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'oval',
  //                     value: 'oval'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'lobular',
  //                     value: 'lobular'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'tubular',
  //                     value: 'tubular'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'loculated',
  //                     value: 'loculated'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'encapsulated',
  //                     value: 'encapsulated',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'septated',
  //                     value: 'septated',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'internal hemorrhage',
  //                     value: 'internal hemorrhage',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'internal calcification',
  //                     value: 'internal calcification',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'central necrosis',
  //                     value: 'central necrosis',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'internal vascularity',
  //                     value: 'internal vascularity',
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'border',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'regular',
  //                     value: 'regular',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'irregular',
  //                     value: 'irregular',
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'location',
  //                 type: 'option',
  //                 multiple: true,
  //                 input: [],
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'head',
  //                     value: 'head',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'neck',
  //                     value: 'neck',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'body',
  //                     value: 'body',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'tail',
  //                     value: 'tail',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'uncinate process',
  //                     value: 'uncinate process',
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'suggestive of',
  //                 type: 'option',
  //                 multiple: true,
  //                 input: [],
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'cyst/pseudocyst',
  //                     value: 'cyst/pseudocyst',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'abscess',
  //                     value: 'abscess',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'malignant lesion/metastasis',
  //                     value: 'malignant',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'being lesion',
  //                     value: 'being lesion',
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'invasion to surrounding tissue',
  //                 type: 'checkbox',
  //                 input: false,
  //                 colValue: '6'
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'sign of inflammation/collection around it',
  //                 type: 'checkbox',
  //                 input: false,
  //                 colValue: '6'
  //               },
  //             ]
  //           },
  //           {
  //             id: uid(25),
  //             label: 'Sign of Pancreatitis',
  //             type: 'checkbox',
  //             input: false,
  //             colValue: '6'
  //           },
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'correlation with CT san is recommended',
  //           },
  //         ]
  //       },
  //     ]
  //   },
  // ]
}

const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value

    }
    state.inputFields = inputFields
  }
}

const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise 'return' will not work properly

    // The node has not been found and we have no more options
    return null
  }
}
const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
