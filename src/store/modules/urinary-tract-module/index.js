import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  disabled: false,
  inputFields: [
    {
      id: uid(25),
      label: 'Ureter',
      type: 'checkbox',
      input: false,
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'Right Ureter',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'Right Ureter Status',
              type: 'option',
              input: 'normal',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Normal(Not Dilated)',
                  value: 'normal',
                },
                {
                  id: uid(25),
                  label: 'Abnormal(Dilated)',
                  value: 'abnormal',
                  children: [
                    {
                      id: uid(25),
                      label: 'Location',
                      type: 'option',
                      input: [],
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'Proximal Part',
                          value: 'Proximal Part',
                        },
                        {
                          id: uid(25),
                          label: 'Mid Part ',
                          value: 'Mid Part ',
                        },
                        {
                          id: uid(25),
                          label: 'Distal Part',
                          value: 'Distal Part',
                        },
                      ]
                    },
                    {
                      id: uid(25),
                      label: 'Sign of stone',
                      type: 'checkbox',
                      input: false,
                      children: [
                        {
                          id: uid(25),
                          label: 'Number',
                          input: '',
                          type: 'option',
                          multiple: false,
                          options: [
                            {
                              id: uid(25),
                              label: 'one',
                              value: 'one',
                            },
                            {
                              id: uid(25),
                              label: 'few',
                              value: 'few',
                            },
                            {
                              id: uid(25),
                              label: 'many',
                              value: 'many',
                            }
                          ]
                        },
                        {
                          id: uid(25),
                          label: 'Size',
                          input: '',
                          type: 'text',
                        },
                        {
                          id: uid(25),
                          label: 'Location',
                          type: 'option',
                          input: [],
                          multiple: true,
                          options: [
                            {
                              id: uid(25),
                              label: 'Proximal Part',
                              value: 'Proximal Part',
                            },
                            {
                              id: uid(25),
                              label: 'Mid Part ',
                              value: 'Mid Part ',
                            },
                            {
                              id: uid(25),
                              label: 'Distal Part',
                              value: 'Distal Part',
                            },
                          ]
                        },
                      ]
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'Total course cannot be fully evaluated due to severe gas shadow.',
                  value: 'Total course cannot be fully evaluated due to severe gas shadow.',
                },
              ]
            },
          ]
        },
        {
          id: uid(25),
          label: 'Left Ureter',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'Left Ureter Status',
              type: 'option',
              input: 'normal',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Normal(Not Dilated)',
                  value: 'normal',
                },
                {
                  id: uid(25),
                  label: 'Abnormal(Dilated)',
                  value: 'abnormal',
                  children: [
                    {
                      id: uid(25),
                      label: 'Location',
                      type: 'option',
                      input: [],
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'Proximal Part',
                          value: 'Proximal Part',
                        },
                        {
                          id: uid(25),
                          label: 'Mid Part ',
                          value: 'Mid Part ',
                        },
                        {
                          id: uid(25),
                          label: 'Distal Part',
                          value: 'Distal Part',
                        },
                      ]
                    },
                    {
                      id: uid(25),
                      label: 'Sign of stone',
                      type: 'checkbox',
                      input: false,
                      children: [
                        {
                          id: uid(25),
                          label: 'Number',
                          input: '',
                          type: 'option',
                          multiple: false,
                          options: [
                            {
                              id: uid(25),
                              label: 'one',
                              value: 'one',
                            },
                            {
                              id: uid(25),
                              label: 'few',
                              value: 'few',
                            },
                            {
                              id: uid(25),
                              label: 'many',
                              value: 'many',
                            }
                          ]
                        },
                        {
                          id: uid(25),
                          label: 'Size',
                          input: '',
                          type: 'text',
                        },
                        {
                          id: uid(25),
                          label: 'Location',
                          type: 'option',
                          input: [],
                          multiple: true,
                          options: [
                            {
                              id: uid(25),
                              label: 'Proximal Part',
                              value: 'Proximal Part',
                            },
                            {
                              id: uid(25),
                              label: 'Mid Part ',
                              value: 'Mid Part ',
                            },
                            {
                              id: uid(25),
                              label: 'Distal Part',
                              value: 'Distal Part',
                            },
                          ]
                        },
                      ]
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'Total course cannot be fully evaluated due to severe gas shadow.',
                  value: 'Total course cannot be fully evaluated due to severe gas shadow.',
                },
              ]
            },
          ]
        },
      ]
    },
    {
      id: uid(25),
      label: 'Urinary Bladder',
      type: 'checkbox',
      input: false,
      colValue: '6',
      children: [{
        id: uid(25),
        label: 'Distension',
        type: 'option',
        input: 'well distended',
        multiple: false,
        options: [
          {
            id: uid(25),
            label: 'Empty',
            value: 'Empty',
            children: [{
              id: uid(25),
              label: 'So (Perfect) evaluation of pelvic organs is not possible.',
              type: 'checkbox',
              input: false,
            }]
          },
          {
            id: uid(25),
            label: 'Semi-distended',
            value: 'Semi-distended',
          },
          {
            id: uid(25),
            label: 'well distended',
            value: 'well distended',
          },
          {
            id: uid(25),
            label: 'Completely distended from suprapubic area to upper umbilical area',
            value: 'Completely distended from suprapubic area to upper umbilical area',
          },
        ]
      },
        {
          id: uid(25),
          label: 'Contains Foley Catheter',
          type: 'checkbox',
          input: false,
          children: [{
            id: uid(25),
            label: 'So evaluation of pelvic organ is impossible.',
            type: 'checkbox',
            input: false,
          }]
        },
        {
          id: uid(25),
          label: 'Wall Thickness',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Normal',
              value: 'normal',
            },
            {
              id: uid(25),
              label: 'Abnormal(Thickened)',
              value: 'Abnormal',
              children: [{
                id: uid(25),
                label: 'Size',
                input: '',
                type: 'text'
              },
                {
                  id: uid(25),
                  type: 'checkbox',
                  input: false,
                  label: 'Correlation with U/A and U/C is recommended.',
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Stone',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'Number of Stone',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'one',
                  value: 'one',
                },
                {
                  id: uid(25),
                  label: 'few',
                  value: 'few',
                },
                {
                  id: uid(25),
                  label: 'many',
                  value: 'many',
                },
              ]
            },
            {
              id: uid(25),
              label: 'size',
              type: 'text',
              input: '',
            }
          ]
        },
        {
          id: uid(25),
          label: 'S.O.L',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'Number',
              input: '',
              type: 'text',
            },
            {
              id: uid(25),
              label: 'Size',
              input: '',
              type: 'text',
            },
            {
              id: uid(25),
              label: 'echogenicity',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'hyperecho',
                  value: 'hyperecho',
                },
                {
                  id: uid(25),
                  label: 'isoecho',
                  value: 'isoecho',
                },
                {
                  id: uid(25),
                  label: 'hypoecho',
                  value: 'hypoecho',
                },
                {
                  id: uid(25),
                  label: 'anechoic',
                  value: 'anechoic',
                },
              ]
            },
            {
              id: uid(25),
              label: 'components',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'cystic',
                  value: 'cystic',
                },
                {
                  id: uid(25),
                  label: 'solid',
                  value: 'solid',
                },
                {
                  id: uid(25),
                  label: 'mixture',
                  value: 'mixture',
                },
              ]
            },
            {
              id: uid(25),
              label: 'morphology',
              input: [],
              type: 'option',
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'spherical',
                  value: 'spherical',
                },
                {
                  id: uid(25),
                  label: 'ovoid',
                  value: 'ovoid',
                },
                {
                  id: uid(25),
                  label: 'polypoid',
                  value: 'polypoid',
                },
                {
                  id: uid(25),
                  label: 'papillary',
                  value: 'papillary',
                },
                {
                  id: uid(25),
                  label: 'lobular',
                  value: 'lobular',
                },
                {
                  id: uid(25),
                  label: 'loculated',
                  value: 'loculated',
                },
                {
                  id: uid(25),
                  label: 'encapsulated',
                  value: 'encapsulated',
                },
                {
                  id: uid(25),
                  label: 'septated',
                  value: 'septated',
                },
                {
                  id: uid(25),
                  label: 'internal vascularity',
                  value: 'internal vascularity',
                },
                {
                  id: uid(25),
                  label: 'hemorrhagic',
                  value: 'hemorrhagic',
                },
                {
                  id: uid(25),
                  label: 'central necrosis',
                  value: 'central necrosis',
                },
              ]
            },
            {
              id: uid(25),
              label: 'Border',
              input: '',
              type: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Regular',
                  value: 'regular'
                },
                {
                  id: uid(25),
                  label: 'Irregular',
                  value: 'irregular'
                }
              ]
            },
            {
              id: uid(25),
              label: 'Location',
              input: [],
              type: 'option',
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'Fundus of Bladder',
                  value: 'fundus of bladder'
                },
                {
                  id: uid(25),
                  label: 'Body',
                  value: 'body',
                  children: [
                    {
                      id: uid(25),
                      label: 'Exact Location in Body',
                      input: [],
                      type: 'option',
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'posterior wall',
                          value: 'posterior wall'
                        },
                        {
                          id: uid(25),
                          label: 'lateral wall',
                          value: 'lateral wall'
                        },
                        {
                          id: uid(25),
                          label: 'anterior wall',
                          value: 'anterior wall'
                        },
                      ]
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'Trigone',
                  value: 'trigone'
                },
                {
                  id: uid(25),
                  label: 'Neck of Bladder',
                  value: 'neck of bladder'
                },
              ]
            },
            {
              id: uid(25),
              type: 'checkbox',
              input: false,
              label: 'Invasion to Surrounding Tissue',
            },
            {
              id: uid(25),
              type: 'text',
              input: '',
              label: 'suggestive of',
            },
          ]
        },
        {
          id: uid(25),
          label: 'mucosal irregularity',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'Post Voiding Residual Bladder Volume(If Evaluated)',
          type: 'text',
          input: '',
        },
      ]
    },
  ]
}
const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value
    }
    state.inputFields = inputFields
  },

  changeDisabled(state, payload) {
    state.disabled = payload;
  }
}
const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  },

  toggleDisabled(context, payload) {
    context.commit("changeDisabled", payload)
  }
}
let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j
      < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }
    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i
      < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j
        < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }
    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly
    // The node has not been found and we have no more options
    return null
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  },

  disabled: (state) => {
    return state.disabled;
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
