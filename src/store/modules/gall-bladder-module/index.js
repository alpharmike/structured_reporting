import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [{
    id: uid(25),
    label: 'distention',
    type: 'option',
    multiple: false,
    colValue: '6',
    input: 'well distended',
    options: [{
      id: uid(25),
      label: 'contracted',
      value: 'contracted'
    },
      {
        id: uid(25),
        label: 'semi distended',
        value: 'semi distended'
      },
      {
        id: uid(25),
        label: 'well distended',
        value: 'well distended'
      },
      {
        id: uid(25),
        label: 'over distended',
        value: 'over distended'
      }
    ]
  },
    {
      id: uid(25),
      label: 'Stone',
      type: 'checkbox',
      colValue: '6',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'number',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'one',
              value: 'one',
            },
            {
              id: uid(25),
              label: 'few',
              value: 'few',
            },
            {
              id: uid(25),
              label: 'many',
              value: 'many',
            },
          ]
        },
        {
          id: uid(25),
          label: 'size up to',
          type: 'text',
          input: ''
        },
      ]
    },
    {
      id: uid(25),
      label: 'wall thickening',
      type: 'option',
      multiple: false,
      input: 'without any sign',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'without any sign',
          value: 'without any sign'
        },
        {
          id: uid(25),
          label: 'upper limit normal',
          value: 'upper limit normal',
          children: [{
            id: uid(25),
            label: 'Size',
            input: '',
            type: 'text'
          }
          ]
        },
        {
          id: uid(25),
          label: 'significant',
          value: 'significant',
          children: [{
            id: uid(25),
            label: 'Size',
            input: '',
            type: 'text'
          }
          ]
        }
      ]
    },
    {
      id: uid(25),
      type: 'option',
      multiple: false,
      input: 'no',
      label: 'evidence of acute cholecystitis',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'no',
          value: 'no'
        },
        {
          id: uid(25),
          label: 'yes',
          value: 'yes'
        },
        {
          id: uid(25),
          label: 'not mentioned',
          value: 'not mentioned'
        }
      ]
    },
    {
      id: uid(25),
      label: 'Morphy sign.',
      type: 'checkbox',
      input: false,
      colValue: '6',
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'sludge',
      colValue: '6',
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'pericholecystic fluid',
      colValue: '6',
    },
  ],
  // inputFields: [
  //   {
  //     id: uid(25),
  //     label: 'evaluation status',
  //     type: 'option',
  //     multiple: false,
  //     input: 'evaluated',
  //     colValue: '12',
  //     options: [
  //       {
  //         id: uid(25),
  //         label: 'is not seen due to previous resection',
  //         value: 'is not seen due to previous resection'
  //       },
  //       {
  //         id: uid(25),
  //         label: 'evaluated',
  //         value: 'evaluated',
  //         children: [{
  //           id: uid(25),
  //           label: 'distention',
  //           type: 'option',
  //           multiple: false,
  //           colValue: '6',
  //           input: 'well distended',
  //           options: [{
  //             id: uid(25),
  //             label: 'contracted',
  //             value: 'contracted'
  //           },
  //             {
  //               id: uid(25),
  //               label: 'semi distended',
  //               value: 'semi distended'
  //             },
  //             {
  //               id: uid(25),
  //               label: 'well distended',
  //               value: 'well distended'
  //             },
  //             {
  //               id: uid(25),
  //               label: 'over distended',
  //               value: 'over distended'
  //             }
  //           ]
  //         },
  //           {
  //             id: uid(25),
  //             label: 'Stone',
  //             type: 'checkbox',
  //             colValue: '6',
  //             input: false,
  //             children: [
  //               {
  //                 id: uid(25),
  //                 label: 'number',
  //                 type: 'option',
  //                 input: '',
  //                 multiple: false,
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'one',
  //                     value: 'one',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'few',
  //                     value: 'few',
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'many',
  //                     value: 'many',
  //                   },
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'size up to',
  //                 type: 'text',
  //                 input: ''
  //               },
  //             ]
  //           },
  //           {
  //             id: uid(25),
  //             label: 'wall thickening',
  //             type: 'option',
  //             multiple: false,
  //             input: 'without any sign',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'without any sign',
  //                 value: 'without any sign'
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'upper limit normal',
  //                 value: 'upper limit normal',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Size',
  //                   input: '',
  //                   type: 'text'
  //                 }
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'significant',
  //                 value: 'significant',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Size',
  //                   input: '',
  //                   type: 'text'
  //                 }
  //                 ]
  //               }
  //             ]
  //           },
  //           {
  //             id: uid(25),
  //             type: 'option',
  //             multiple: false,
  //             input: 'no',
  //             label: 'evidence of acute cholecystitis',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'no',
  //                 value: 'no'
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'yes',
  //                 value: 'yes'
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'not mentioned',
  //                 value: 'not mentioned'
  //               }
  //             ]
  //           },
  //           {
  //             id: uid(25),
  //             label: 'Morphy sign.',
  //             type: 'checkbox',
  //             input: false,
  //             colValue: '6',
  //           },
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'sludge',
  //             colValue: '6',
  //           },
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'pericholecystic fluid',
  //             colValue: '6',
  //           },
  //         ]
  //       }
  //
  //     ]
  //   },
  // ]
}

const mutations = {
  updateFieldValue(state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value

    }
    state.inputFields = inputFields
  }
}

const actions = {
  updateLiverValue(context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode(context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions(context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly

    // The node has not been found and we have no more options
    return null
  }
}
const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if ('multiple' in node && node.multiple && val.input.includes(node.options[i])) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      } else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
