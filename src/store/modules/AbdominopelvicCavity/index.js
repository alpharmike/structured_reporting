import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'free fluid',
      type: 'checkbox',
      input: false,
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'severity',
          type: 'option',
          multiple: false,
          input: '',
          options: [
            {
              id: uid(25),
              label: 'minimal',
              value: 'minimal'
            },
            {
              id: uid(25),
              label: 'mild',
              value: 'mild'
            },
            {
              id: uid(25),
              label: 'moderate',
              value: 'moderate'
            },
            {
              id: uid(25),
              label: 'severe',
              value: 'severe'
            }
          ]
        },
        {
          id: uid(25),
          label: 'location',
          type: 'option',
          multiple: false,
          input: '',
          options: [
            {
              id: uid(25),
              label: 'generalized',
              value: 'generalized'
            },
            {
              id: uid(25),
              label: 'localized or more prominent in',
              value: 'localized or more prominent in',
              children: [
                {
                  id: uid(25),
                  label: 'exact locations',
                  type: 'option',
                  multiple: true,
                  input: [],
                  options: [
                    {
                      id: uid(25),
                      label: 'RUQ',
                      value: 'RUQ'
                    },
                    {
                      id: uid(25),
                      label: 'RLQ',
                      value: 'RLQ'
                    },
                    {
                      id: uid(25),
                      label: 'LUQ',
                      value: 'LUQ'
                    },
                    {
                      id: uid(25),
                      label: 'LLQ',
                      value: 'LLQ'
                    },
                    {
                      id: uid(25),
                      label: 'interloopal area',
                      value: 'interloopal area'
                    },
                    {
                      id: uid(25),
                      label: 'douglas pouch',
                      value: 'douglas pouch'
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'fluid morphology',
          type: 'option',
          multiple: false,
          input: '',
          options: [
            {
              id: uid(25),
              label: 'suppurative',
              value: 'suppurative'
            },
            {
              id: uid(25),
              label: 'hemorrhagic',
              value: 'hemorrhagic'
            },
            {
              id: uid(25),
              label: 'non suppurative & non hemorrhagic',
              value: 'non suppurative & non hemorrhagic',
            }
          ]
        },
        {
          id: uid(25),
          label: 'sign of fat inflammation around it',
          type: 'checkbox',
          input: false
        }
      ]
    },
    {
      id: uid(25),
      label: 'mass/abscess/fluid collection',
      type: 'checkbox',
      input: false,
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'number',
          type: 'option',
          multiple: false,
          input: '',
          options: [
            {
              id: uid(25),
              label: 'one',
              value: 'one'
            },
            {
              id: uid(25),
              label: 'few',
              value: 'few'
            },
            {
              id: uid(25),
              label: 'many',
              value: 'many'
            },
          ]
        },
        {
          id: uid(25),
          label: 'components',
          type: 'option',
          multiple: false,
          input: '',
          options: [
            {
              id: uid(25),
              label: 'cystic',
              value: 'cystic'
            },
            {
              id: uid(25),
              label: 'solid',
              value: 'solid'
            },
            {
              id: uid(25),
              label: 'mix',
              value: 'mix'
            }
          ]
        },
        {
          id: uid(25),
          label: 'morphology',
          multiple: true,
          type: 'option',
          input: [],
          options: [
            {
              id: uid(25),
              label: 'spherical',
              value: 'spherical'
            },
            {
              id: uid(25),
              label: 'oval',
              value: 'oval'
            },
            {
              id: uid(25),
              label: 'encapsulated ',
              value: 'encapsulated ',
              children: [
                {
                  id: uid(25),
                  label: 'condition(thickness) ',
                  type: 'option',
                  multiple: false,
                  input: '',
                  options: [
                    {
                      id: uid(25),
                      label: 'thin wall',
                      value: 'thin wall'
                    },
                    {
                      id: uid(25),
                      label: 'thick wall',
                      value: 'thick wall'
                    }
                  ]
                }
              ]
            },
            {
              id: uid(25),
              label: 'lobular',
              value: 'lobular'
            },
            {
              id: uid(25),
              label: 'tubular',
              value: 'tubular'
            },
            {
              id: uid(25),
              label: 'loculated',
              value: 'loculated'
            },
            {
              id: uid(25),
              label: 'internal septation',
              value: 'internal septation'
            },
            {
              id: uid(25),
              label: 'hemorrhagic',
              value: 'hemorrhagic'
            },
            {
              id: uid(25),
              label: 'central necrosis',
              value: 'central necrosis'
            },
            {
              id: uid(25),
              label: 'internal calcification',
              value: 'internal calcification'
            },
            {
              id: uid(25),
              label: 'internal vascularity',
              value: 'internal vascularity'
            }
          ]
        },
        {
          id: uid(25),
          label: 'border',
          type: 'option',
          multiple: false,
          input: '',
          options: [
            {
              id: uid(25),
              label: 'regular',
              value: 'regular'
            },
            {
              id: uid(25),
              label: 'irregular',
              value: 'irregular'
            }
          ]
        },
        {
          id: uid(25),
          label: 'echogenicity',
          type: 'option',
          multiple: false,
          input: '',
          options: [
            {
              id: uid(25),
              label: 'hypoechoic',
              value: 'hypoechoic'
            },
            {
              id: uid(25),
              label: 'hyperechoic',
              value: 'hyperechoic'
            },
            {
              id: uid(25),
              label: 'isoechoic',
              value: 'isoechoic'
            },
            {
              id: uid(25),
              label: 'nonechoic',
              value: 'nonechoic'
            }
          ]
        },
        {
          id: uid(25),
          label: 'internal echogenicity',
          multiple: false,
          type: 'option',
          input: '',
          options: [
            {
              id: uid(25),
              label: 'homogeneous',
              value: 'homogeneous'
            },
            {
              id: uid(25),
              label: 'heterogeneous',
              value: 'heterogeneous'
            }
          ]
        },
        {
          id: uid(25),
          label: 'size',
          type: 'text',
          input: ''
        },
        {
          id: uid(25),
          label: 'location',
          type: 'option',
          multiple: true,
          input: [],
          options: [
            {
              id: uid(25),
              label: 'RUQ',
              value: 'RUQ'
            },
            {
              id: uid(25),
              label: 'RLQ',
              value: 'RLQ'
            },
            {
              id: uid(25),
              label: 'LUQ',
              value: 'LUQ'
            },
            {
              id: uid(25),
              label: 'LLQ',
              value: 'LLQ'
            },
            {
              id: uid(25),
              label: 'adjacent to',
              value: 'adjacent to',
              children: [
                {
                  id: uid(25),
                  label: 'adjacent to organs',
                  type: 'option',
                  input: [],
                  multiple: true,
                  options: [
                    {
                      id: uid(25),
                      label: 'liver',
                      value: 'liver'
                    },
                    {
                      id: uid(25),
                      label: 'gall bladder',
                      value: 'gall bladder'
                    },
                    {
                      id: uid(25),
                      label: 'stomach',
                      value: 'stomach'
                    },
                    {
                      id: uid(25),
                      label: 'duodenum',
                      value: 'duodenum'
                    },
                    {
                      id: uid(25),
                      label: 'pancreas',
                      value: 'pancreas'
                    },
                    {
                      id: uid(25),
                      label: 'spleen',
                      value: 'spleen'
                    },
                    {
                      id: uid(25),
                      label: 'right or left kidney',
                      value: 'right or left kidney'
                    },
                    {
                      id: uid(25),
                      label: 'right or left adrenal',
                      value: 'right or left adrenal'
                    },
                    {
                      id: uid(25),
                      label: 'bladder',
                      value: 'bladder'
                    },
                    {
                      id: uid(25),
                      label: 'small intestine',
                      value: 'small intestine'
                    },
                    {
                      id: uid(25),
                      label: 'colon',
                      value: 'colon'
                    },
                    {
                      id: uid(25),
                      label: 'appendix',
                      value: 'appendix'
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'invasion to surrounding tissue',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'sign of inflammation/fluid collection around it',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'D.D',
          type: 'option',
          multiple: true,
          input: [],
          options: [
            {
              id: uid(25),
              label: 'abscess',
              value: 'abscess'
            },
            {
              id: uid(25),
              label: 'loculated fluid collection/cyst',
              value: 'loculated fluid collection/cyst'
            },
            {
              id: uid(25),
              label: 'pseudocyst',
              value: 'pseudocyst'
            },
            {
              id: uid(25),
              label: 'hematoma',
              value: 'hematoma'
            },
            {
              id: uid(25),
              label: 'peritoneal inclusion cyst',
              value: 'peritoneal inclusion cyst'
            },
            {
              id: uid(25),
              label: 'malignant lesion/metastasis',
              value: 'malignant lesion/metastasis'
            },
            {
              id: uid(25),
              label: 'benign lesion',
              value: 'benign lesion'
            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'lymphadenopathy',
      type: 'checkbox',
      input: false,
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'number',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'one',
              value: 'one',
            },
            {
              id: uid(25),
              label: 'few',
              value: 'few',
            },
            {
              id: uid(25),
              label: 'many',
              value: 'many',
            },
          ]
        },
        {
          id: uid(25),
          label: 'location',
          type: 'option',
          multiple: true,
          input: [],
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'paraaortic',
              value: 'paraaortic'
            },
            {
              id: uid(25),
              label: 'illiac',
              value: 'illiac'
            },
            {
              id: uid(25),
              label: 'inguinal',
              value: 'inguinal'
            },
            {
              id: uid(25),
              label: 'mesentric',
              value: 'mesentric'
            },
            {
              id: uid(25),
              label: 'splenic',
              value: 'splenic'
            },
            {
              id: uid(25),
              label: 'portal',
              value: 'portal'
            },
            {
              id: uid(25),
              label: 'celiac',
              value: 'celiac'
            }
          ]
        },
        {
          id: uid(25),
          label: 'suggestive of',
          type: 'option',
          multiple: true,
          input: [],
          options: [
            {
              id: uid(25),
              label: 'infection',
              value: 'infection'
            },
            {
              id: uid(25),
              label: 'malignant process/metastasis',
              value: 'malignant process/metastasis'
            }
          ]
        },
        {
          id: uid(25),
          label: 'size',
          type: 'text',
          input: '',
          colValue: '12',
        },
      ]
    },
    {
      id: uid(25),
      label: 'site of surgery',
      type: 'option',
      multiple: false,
      input: 'normal',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal'
        },
        {
          id: uid(25),
          label: 'abnormal',
          value: 'abnormal',
          children: [
            {
              id: uid(25),
              label: 'inflammation',
              type: 'checkbox',
              input: false
            },
            {
              id: uid(25),
              label: 'lesion',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'number',
                  type: 'option',
                  input: '',
                  multiple: false,
                  colValue: '6',
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one',
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few',
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many',
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'echogenicity',
                  type: 'option',
                  multiple: false,
                  input: '',
                  colValue: '6',
                  options: [
                    {
                      label: 'hyperechoic',
                      value: 'hyperechoic'
                    },
                    {
                      label: 'hypoechoic',
                      value: 'hypoehoic'
                    },
                    {
                      label: 'isoechoic',
                      value: 'isoechoic'
                    },
                    {
                      label: 'nonechoic',
                      value: 'nonechoic'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'suggestive of',
                  type: 'option',
                  multiple: true,
                  input: [],
                  colValue: '12',
                  options: [
                    {
                      id: uid(25),
                      label: 'fluid collection',
                      value: 'fluid collectionc'
                    },
                    {
                      id: uid(25),
                      label: 'abscess',
                      value: 'abscess'
                    },
                    {
                      id: uid(25),
                      label: 'hematoma',
                      value: 'hematoma'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'size',
                  type: 'text',
                  input: '',
                  colValue: '6',
                },
                {
                  id: uid(25),
                  label: 'distance from skin',
                  type: 'text',
                  input: '',
                  colValue: '6',
                },
              ]
            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'for better evaluation, if clinically indicated, correlation with CT scan is recommended',
      type: 'checkbox',
      input: false,
      colValue: '12'
    }
  ]
}

const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value

    }
    state.inputFields = inputFields
  }
}

const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly

    // The node has not been found and we have no more options
    return null
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
