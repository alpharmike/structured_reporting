import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  disabled: false,
  inputFields: [
    {
      id: uid(25),
      label: 'Right Ureter',
      type: 'checkbox',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'Right Ureter Status',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Normal(Not Dilated)',
              value: 'normal',
            },
            {
              id: uid(25),
              label: 'Abnormal(Dilated)',
              value: 'abnormal',
              children: [
                {
                  id: uid(25),
                  label: 'Location',
                  type: 'option',
                  input: [],
                  multiple: true,
                  options: [
                    {
                      id: uid(25),
                      label: 'Proximal Part',
                      value: 'Proximal Part',
                    },
                    {
                      id: uid(25),
                      label: 'Mid Part ',
                      value: 'Mid Part ',
                    },
                    {
                      id: uid(25),
                      label: 'Distal Part',
                      value: 'Distal Part',
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'Sign of stone',
                  type: 'checkbox',
                  input: false,
                  children: [
                    {
                      id: uid(25),
                      label: 'Number',
                      input: '',
                      type: 'option',
                      multiple: false,
                      options: [
                        {
                          id: uid(25),
                          label: 'one',
                          value: 'one',
                        },
                        {
                          id: uid(25),
                          label: 'few',
                          value: 'few',
                        },
                        {
                          id: uid(25),
                          label: 'many',
                          value: 'many',
                        }
                      ]
                    },
                    {
                      id: uid(25),
                      label: 'Size up to',
                      input: '',
                      type: 'text',
                    },
                    {
                      id: uid(25),
                      label: 'Location',
                      type: 'option',
                      input: [],
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'Proximal Part',
                          value: 'Proximal Part',
                        },
                        {
                          id: uid(25),
                          label: 'Mid Part ',
                          value: 'Mid Part ',
                        },
                        {
                          id: uid(25),
                          label: 'Distal Part',
                          value: 'Distal Part',
                        },
                      ]
                    },
                  ]
                }
              ]
            },
            {
              id: uid(25),
              label: 'Total course cannot be fully evaluated due to severe gas shadow.',
              value: 'Total course cannot be fully evaluated due to severe gas shadow.',
            },
          ]
        },
      ]
    },
    {
      id: uid(25),
      label: 'Left Ureter',
      type: 'checkbox',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'Left Ureter Status',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'Normal(Not Dilated)',
              value: 'normal',
            },
            {
              id: uid(25),
              label: 'Abnormal(Dilated)',
              value: 'abnormal',
              children: [
                {
                  id: uid(25),
                  label: 'Location',
                  type: 'option',
                  input: [],
                  multiple: true,
                  options: [
                    {
                      id: uid(25),
                      label: 'Proximal Part',
                      value: 'Proximal Part',
                    },
                    {
                      id: uid(25),
                      label: 'Mid Part ',
                      value: 'Mid Part ',
                    },
                    {
                      id: uid(25),
                      label: 'Distal Part',
                      value: 'Distal Part',
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'Sign of stone',
                  type: 'checkbox',
                  input: false,
                  children: [
                    {
                      id: uid(25),
                      label: 'Number',
                      input: '',
                      type: 'option',
                      multiple: false,
                      options: [
                        {
                          id: uid(25),
                          label: 'one',
                          value: 'one',
                        },
                        {
                          id: uid(25),
                          label: 'few',
                          value: 'few',
                        },
                        {
                          id: uid(25),
                          label: 'many',
                          value: 'many',
                        }
                      ]
                    },
                    {
                      id: uid(25),
                      label: 'Size up to',
                      input: '',
                      type: 'text',
                    },
                    {
                      id: uid(25),
                      label: 'Location',
                      type: 'option',
                      input: [],
                      multiple: true,
                      options: [
                        {
                          id: uid(25),
                          label: 'Proximal Part',
                          value: 'Proximal Part',
                        },
                        {
                          id: uid(25),
                          label: 'Mid Part ',
                          value: 'Mid Part ',
                        },
                        {
                          id: uid(25),
                          label: 'Distal Part',
                          value: 'Distal Part',
                        },
                      ]
                    },
                  ]
                }
              ]
            },
            {
              id: uid(25),
              label: 'Total course cannot be fully evaluated due to severe gas shadow.',
              value: 'Total course cannot be fully evaluated due to severe gas shadow.',
            },
          ]
        },
      ]
    },
  ]
}
const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value
    }
    state.inputFields = inputFields
  },

  changeDisabled(state, payload) {
    state.disabled = payload;
  }
}
const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },
  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  },

  toggleDisabled(context, payload) {
    context.commit("changeDisabled", payload)
  }
}
let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j
      < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }
    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i
      < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j
        < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }
    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly
    // The node has not been found and we have no more options
    return null
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  },

  disabled: (state) => {
    return state.disabled;
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
