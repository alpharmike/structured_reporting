import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'Abdominal Aorta',
      value: 'abdominal-aorta',
      type: 'option',
      input: '',
      multiple: false,
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'Calibre',
          value: 'calibre',
          children: [
            {
              id: uid(25),
              label: 'calibre status',
              value: 'calibre-status',
              type: 'option',
              input: 'normal',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Normal',
                  value: 'normal',
                },
                {
                  id: uid(25),
                  label: 'Abnormal',
                  value: 'abnormal',
                  children: [
                    {
                      id: uid(25),
                      label: 'Abnormal Type',
                      value: 'abnormal-type',
                      input: '',
                      type: 'option',
                      multiple: false,
                      options: [
                        {
                          id: uid(25),
                          label: 'Luminal Narrowing',
                          value: 'luminal narrowing',
                          children: [
                            {
                              id: uid(25),
                              label: 'Luminal Narrowing Status',
                              value: 'luminal-narrowing-status',
                              input: [],
                              type: 'option',
                              multiple: true,
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Thrombose/Clot',
                                  value: 'thrombose-clot',
                                  children: [
                                    {
                                      id: uid(25),
                                      label: 'Stenosis Percentage',
                                      value: 'stenosis-percentage',
                                      type: 'text',
                                      input: '',
                                    },
                                    {
                                      id: uid(25),
                                      label: 'Location',
                                      value: 'location',
                                      input: [],
                                      type: 'option',
                                      multiple: true,
                                      options: [
                                        {
                                          id: uid(25),
                                          label: 'Suprarenal Aorta',
                                          value: 'suprarenal-aorta',
                                        },
                                        {
                                          id: uid(25),
                                          label: 'Infrarenal Aorta',
                                          value: 'infrarenal-aorta',
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  id: uid(25),
                                  label: 'Atherosclerosis',
                                  value: 'atherosclerosis',
                                  children: [
                                    {
                                      id: uid(25),
                                      label: 'Stenosis Percentage',
                                      value: 'stenosis-percentage',
                                      type: 'text',
                                      input: '',
                                    },
                                  ]
                                },
                                {
                                  id: uid(25),
                                  label: 'Idiopathic',
                                  value: 'idiopathic',
                                },
                              ]
                            }
                          ],
                        },
                        {
                          id: uid(25),
                          label: 'Luminal Dilatation',
                          value: 'luminal-dilatation',
                          children: [
                            {
                              id: uid(25),
                              label: 'Luminal Dilatation Status',
                              value: 'luminal-dilatation-status',
                              input: [],
                              type: 'option',
                              multiple: true,
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Aneurysm',
                                  value: 'aneurysm',
                                  children: [
                                    {
                                      id: uid(25),
                                      label: 'Size in MM',
                                      value: 'size',
                                      input: '',
                                      type: 'text',
                                    },
                                    {
                                      id: uid(25),
                                      label: 'Location',
                                      value: 'location',
                                      input: [],
                                      type: 'option',
                                      multiple: true,
                                      options: [
                                        {
                                          id: uid(25),
                                          label: 'Suprarenal Aorta',
                                          value: 'suprarenal-aorta',
                                        },
                                        {
                                          id: uid(25),
                                          label: 'Infrarenal Aorta',
                                          value: 'infrarenal-aorta',
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  id: uid(25),
                                  label: 'Idiopathic',
                                  value: 'idiopathic',
                                },
                              ]
                            }
                          ]
                        },
                      ]
                    }
                  ]
                },
              ]
            },
          ],
        },
        {
          id: uid(),
          label: 'Coarse',
          value: 'coarse',
          children: [
            {
              id: uid(25),
              label: 'Coarse Status',
              value: 'coarse-status',
              type: 'option',
              input: 'normal',
              multiple: false,
              options: [
                {
                  id: uid(),
                  label: 'Normal',
                  value: 'normal',
                },
                {
                  id: uid(),
                  label: 'Abnormal',
                  value: 'abnormal',
                },
              ]
            }
          ]
        },
        {
          id: uid(),
          label: 'Displaced',
          value: 'displaced',
        },
      ]
    },
    {
      id: uid(25),
      label: 'IVC',
      value: 'ivc',
      type: 'option',
      input: '',
      multiple: false,
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'Calibre',
          value: 'calibre',
          children: [
            {
              id: uid(25),
              label: 'Calibre Status',
              type: 'option',
              input: 'normal',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'Normal',
                  value: 'normal',
                },
                {
                  id: uid(25),
                  label: 'Abnormal',
                  value: 'abnormal',
                  children: [
                    {
                      id: uid(25),
                      label: 'Abnormal Type',
                      value: 'abnormal-type',
                      input: '',
                      type: 'option',
                      multiple: false,
                      options: [
                        {
                          id: uid(25),
                          label: 'Luminal Narrowing',
                          value: 'luminal-narrowing',
                          children: [
                            {
                              id: uid(25),
                              label: 'Luminal Narrowing Status',
                              value: 'luminal-narrowing-status',
                              input: [],
                              type: 'option',
                              multiple: true,
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Thrombose/Clot',
                                  value: 'thrombose-clot',
                                  children: [
                                    {
                                      id: uid(25),
                                      label: 'Stenosis Percentage',
                                      value: 'stenosis-percentage',
                                      type: 'text',
                                      input: '',
                                    },
                                    {
                                      id: uid(25),
                                      label: 'Location',
                                      value: 'location',
                                      input: [],
                                      type: 'option',
                                      multiple: true,
                                      options: [
                                        {
                                          id: uid(25),
                                          label: 'Suprarenal Aorta',
                                          value: 'suprarenal-aorta',
                                        },
                                        {
                                          id: uid(25),
                                          label: 'Infrarenal Aorta',
                                          value: 'infrarenal-aorta',
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  id: uid(25),
                                  label: 'Idiopathic',
                                  value: 'idiopathic',
                                },
                              ]
                            }
                          ],
                        },
                        {
                          id: uid(25),
                          label: 'Luminal Dilatation',
                          value: 'luminal-dilatation',
                          children: [
                            {
                              id: uid(25),
                              label: 'Luminal Dilatation Status',
                              value: 'luminal-dilatation-status',
                              input: [],
                              type: 'option',
                              multiple: true,
                              options: [
                                {
                                  id: uid(25),
                                  label: 'Aneurysm',
                                  value: 'aneurysm',
                                  children: [
                                    {
                                      id: uid(25),
                                      label: 'Size in MM',
                                      value: 'size',
                                      input: '',
                                      type: 'text',
                                    },
                                    {
                                      id: uid(25),
                                      label: 'Location',
                                      value: 'location',
                                      input: [],
                                      type: 'option',
                                      multiple: true,
                                      options: [
                                        {
                                          id: uid(25),
                                          label: 'Suprarenal Aorta',
                                          value: 'suprarenal-aorta',
                                        },
                                        {
                                          id: uid(25),
                                          label: 'Infrarenal Aorta',
                                          value: 'infrarenal-aorta',
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  id: uid(25),
                                  label: 'Idiopathic',
                                  value: 'idiopathic',
                                },
                              ]
                            }
                          ]
                        },
                      ]
                    }
                  ]
                },
              ]
            },
          ],
        },
        {
          id: uid(),
          label: 'Coarse',
          value: 'coarse',
          children: [
            {
              id: uid(25),
              label: 'Coarse Status',
              value: 'coarse-status',
              type: 'option',
              input: 'normal',
              multiple: false,
              options: [
                {
                  id: uid(),
                  label: 'Normal',
                  value: 'normal',
                },
                {
                  id: uid(),
                  label: 'Abnormal',
                  value: 'abnormal',
                },
              ]
            }
          ]
        },
        {
          id: uid(),
          label: 'Displaced',
          value: 'displaced',
        },
      ]
    },
  ]
}

const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value

    }
    state.inputFields = inputFields
  }
}

const actions = {
  updateVasculatureValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise 'return' will not work properly

    // The node has not been found and we have no more options
    return null
  }
}
const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}
const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
