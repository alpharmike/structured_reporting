import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'Transplanted',
      type: 'checkbox',
      input: false,
      colValue: '12',
      children: [
        {
          id: uid(25),
          label: 'position',
          type: 'option',
          input: '',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'RLQ',
              value: 'RLQ'
            }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'right',
      input: false,
      type: 'checkbox',
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'size',
          type: 'option',
          input: 'normal',
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'small',
              value: 'small'
            },
            {
              id: uid(25),
              label: 'enlarged',
              value: 'enlarged'
            }
          ]
        },
        {
          id: uid(25),
          label: 'measurement',
          input: '',
          type: 'text'
        },
        {
          id: uid(25),
          label: 'shape/position',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'abnormal',
              value: 'abnormal',
              children: [
                {
                  id: uid(25),
                  label: 'Abnormality Type',
                  type: 'option',
                  input: [],
                  multiple: true,
                  options: [
                    {
                      id: uid(25),
                      label: 'duplex',
                      value: 'duplex'
                    },
                    {
                      id: uid(25),
                      label: 'horseshoe',
                      value: 'horseshoe'
                    },
                    {
                      id: uid(25),
                      label: 'renal ectopia',
                      value: 'renal ectopia'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'any comment',
                  type: 'text',
                  input: '',
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Corticomedullary Differentiation',
          type: 'option',
          input: 'maintained',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'maintained(normal)',
              value: 'maintained'
            },
            {
              id: uid(25),
              label: 'diminished',
              value: 'diminished'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Cortical Parenchymal Echogenicity',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Minimally Increased',
              value: 'Minimally Increased',
              children: [{
                id: uid(25),
                type: 'checkbox',
                input: false,
                label: 'Suggestive for Parenchymal damage; correlation with BUN/Cr is recommended.'
              }]
            },
            {
              id: uid(25),
              label: 'Increased',
              value: 'Increased',
              children: [{
                id: uid(25),
                type: 'checkbox',
                input: false,
                label: 'Suggestive for Parenchymal damage; correlation with BUN/Cr is recommended'
              }]
            },
            {
              id: uid(25),
              label: 'Severely Increased',
              value: 'Severely Increased',
              children: [{
                id: uid(25),
                type: 'checkbox',
                input: false,
                label: 'Suggestive for Parenchymal damage; correlation with BUN/Cr is recommended'
              }]
            }
          ]
        },
        {
          id: uid(25),
          label: 'sign of cortical thinning',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'stone',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'upper pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'mild pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'lower pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
          ]
        },
        {
          id: uid(25),
          label: 'gravel',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'upper pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'mild pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'lower pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
          ]
        },
        {
          id: uid(25),
          label: 'calcification',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'upper pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'mild pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'lower pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
          ]
        },
        {
          id: uid(25),
          label: 'stasis',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'severity',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'mild',
                  value: 'mild'
                },
                {
                  id: uid(25),
                  label: 'moderate',
                  value: 'moderate'
                },
                {
                  id: uid(25),
                  label: 'severe',
                  value: 'severe'
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'perinehric collection',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'cortical cyst',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'quantity',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'one',
                  value: 'one',
                  children: [
                    {
                      id: uid(25),
                      label: 'size up to',
                      type: 'text',
                      input: ''
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'a few',
                  value: 'a few',
                  children: [
                    {
                      id: uid(25),
                      label: 'size up to',
                      type: 'text',
                      input: ''
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'many',
                  value: 'many',
                  children: [
                    {
                      id: uid(25),
                      label: 'size up to',
                      type: 'text',
                      input: ''
                    }
                  ]
                }]
            },
            {
              id: uid(25),
              label: 'type',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'simple',
                  value: 'simple'
                },
                {
                  id: uid(25),
                  label: 'minimally complex',
                  value: 'minimally complex'
                },
                {
                  id: uid(25),
                  label: 'complex',
                  value: 'complex'
                }]
            },
            {
              id: uid(25),
              label: 'location',
              type: 'option',
              input: [],
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'upper pole',
                  value: 'upper pole'
                },
                {
                  id: uid(25),
                  label: 'mid pole',
                  value: 'mid pole'
                },
                {
                  id: uid(25),
                  label: 'lower pole',
                  value: 'lower pole'
                }]
            }
          ]
        },
        {
          id: uid(25),
          label: 'S.O.L',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'number',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'one',
                  value: 'one'
                },
                {
                  id: uid(25),
                  label: 'few',
                  value: 'few'
                },
                {
                  id: uid(25),
                  label: 'many',
                  value: 'many'
                },
              ]
            },
            {
              id: uid(25),
              label: 'size up to',
              type: 'text',
              input: ''
            },
            {
              id: uid(25),
              label: 'components',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'cystic',
                  value: 'cystic'
                },
                {
                  id: uid(25),
                  label: 'solid',
                  value: 'solid'
                },
                {
                  id: uid(25),
                  label: 'mixture',
                  value: 'mixture'
                }
              ]
            },
            {
              id: uid(25),
              label: 'echogenicity',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'hyperechoic',
                  value: 'hyperechoic'
                },
                {
                  id: uid(25),
                  label: 'isoechoic',
                  value: 'isoechoic'
                },
                {
                  id: uid(25),
                  label: 'hypoechoic',
                  value: 'hypoechoic'
                },
                {
                  id: uid(25),
                  label: 'anechoic',
                  value: 'anechoic'
                }
              ]
            },
            {
              id: uid(25),
              label: 'morphology',
              type: 'option',
              input: [],
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'lobular',
                  value: 'lobular'
                },
                {
                  id: uid(25),
                  label: 'loculated',
                  value: 'loculated'
                },
                {
                  id: uid(25),
                  label: 'encapsulated',
                  value: 'encapsulated'
                },
                {
                  id: uid(25),
                  label: 'septated',
                  value: 'septated'
                },
                {
                  id: uid(25),
                  label: 'internal calcification',
                  value: 'internal calcification'
                },
                {
                  id: uid(25),
                  label: 'fat component',
                  value: 'fat component'
                },
                {
                  id: uid(25),
                  label: 'hemorhage',
                  value: 'hemorhage'
                }
              ]
            },
            {
              id: uid(25),
              label: 'border',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'regular',
                  value: 'regular'
                },
                {
                  id: uid(25),
                  label: 'irregular',
                  value: 'irregular'
                }
              ]
            },
            {
              id: uid(25),
              label: 'location',
              type: 'option',
              input: [],
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'upper pole',
                  value: 'upper pole'
                },
                {
                  id: uid(25),
                  label: 'mid pole',
                  value: 'mid pole'
                },
                {
                  id: uid(25),
                  label: 'lower pole',
                  value: 'lower pole'
                }
              ]
            },
            {
              id: uid(25),
              label: 'invasion to surrounding tissue',
              type: 'checkbox',
              input: false,
            }

          ]
        },
        {
          id: uid(25),
          label: 'Internal Debris, Possibility of Inflammatory Process, Correlation with Bun/Cr',
          type: 'checkbox',
          input: false
        },
        {
          id: uid(25),
          label: 'in favor of',
          type: 'text',
          input: ''
        }
      ]
    },
    {
      id: uid(25),
      label: 'left',
      input: false,
      type: 'checkbox',
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'size',
          type: 'option',
          input: 'normal',
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'small',
              value: 'small'
            },
            {
              id: uid(25),
              label: 'enlarged',
              value: 'enlarged'
            }
          ]
        },
        {
          id: uid(25),
          label: 'measurement',
          input: '',
          type: 'text'
        },
        {
          id: uid(25),
          label: 'shape/position',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'abnormal',
              value: 'abnormal',
              children: [
                {
                  id: uid(25),
                  label: 'Abnormality Type',
                  type: 'option',
                  input: [],
                  multiple: true,
                  options: [
                    {
                      id: uid(25),
                      label: 'duplex',
                      value: 'duplex'
                    },
                    {
                      id: uid(25),
                      label: 'horseshoe',
                      value: 'horseshoe'
                    },
                    {
                      id: uid(25),
                      label: 'renal ectopia',
                      value: 'renal ectopia'
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'any comment',
                  type: 'text',
                  input: '',
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'Corticomedullary Differentiation',
          type: 'option',
          input: 'maintained',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'maintained(normal)',
              value: 'maintained'
            },
            {
              id: uid(25),
              label: 'diminished',
              value: 'diminished'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Cortical Parenchymal Echogenicity',
          type: 'option',
          input: 'normal',
          multiple: false,
          options: [
            {
              id: uid(25),
              label: 'normal',
              value: 'normal'
            },
            {
              id: uid(25),
              label: 'Minimally Increased',
              value: 'Minimally Increased',
              children: [{
                id: uid(25),
                type: 'checkbox',
                input: false,
                label: 'Suggestive for Parenchymal damage; correlation with BUN/Cr is recommended.'
              }]
            },
            {
              id: uid(25),
              label: 'Increased',
              value: 'Increased',
              children: [{
                id: uid(25),
                type: 'checkbox',
                input: false,
                label: 'Suggestive for Parenchymal damage; correlation with BUN/Cr is recommended'
              }]
            },
            {
              id: uid(25),
              label: 'Severely Increased',
              value: 'Severely Increased',
              children: [{
                id: uid(25),
                type: 'checkbox',
                input: false,
                label: 'Suggestive for Parenchymal damage; correlation with BUN/Cr is recommended'
              }]
            }
          ]
        },
        {
          id: uid(25),
          label: 'sign of cortical thinning',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'stone',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'upper pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'mild pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'lower pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
          ]
        },
        {
          id: uid(25),
          label: 'gravel',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'upper pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'mild pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'lower pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
          ]
        },
        {
          id: uid(25),
          label: 'calcification',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'upper pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'mild pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
            {
              id: uid(25),
              label: 'lower pole',
              type: 'checkbox',
              input: false,
              children: [
                {
                  id: uid(25),
                  label: 'quantity',
                  type: 'option',
                  input: '',
                  multiple: false,
                  options: [
                    {
                      id: uid(25),
                      label: 'one',
                      value: 'one'
                    },
                    {
                      id: uid(25),
                      label: 'few',
                      value: 'few'
                    },
                    {
                      id: uid(25),
                      label: 'many',
                      value: 'many'
                    },
                  ]
                },
                {
                  id: uid(25),
                  label: 'size up to',
                  type: 'text',
                  input: ''
                },
              ]
            },
          ]
        },
        {
          id: uid(25),
          label: 'stasis',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'severity',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'mild',
                  value: 'mild'
                },
                {
                  id: uid(25),
                  label: 'moderate',
                  value: 'moderate'
                },
                {
                  id: uid(25),
                  label: 'severe',
                  value: 'severe'
                }
              ]
            }
          ]
        },
        {
          id: uid(25),
          label: 'perinehric collection',
          type: 'checkbox',
          input: false,
        },
        {
          id: uid(25),
          label: 'cortical cyst',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'quantity',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'one',
                  value: 'one',
                  children: [
                    {
                      id: uid(25),
                      label: 'size up to',
                      type: 'text',
                      input: ''
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'a few',
                  value: 'a few',
                  children: [
                    {
                      id: uid(25),
                      label: 'size up to',
                      type: 'text',
                      input: ''
                    }
                  ]
                },
                {
                  id: uid(25),
                  label: 'many',
                  value: 'many',
                  children: [
                    {
                      id: uid(25),
                      label: 'size up to',
                      type: 'text',
                      input: ''
                    }
                  ]
                }]
            },
            {
              id: uid(25),
              label: 'type',
              type: 'option',
              input: 'option',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'simple',
                  value: 'simple'
                },
                {
                  id: uid(25),
                  label: 'minimally complex',
                  value: 'minimally complex'
                },
                {
                  id: uid(25),
                  label: 'complex',
                  value: 'complex'
                }]
            },
            {
              id: uid(25),
              label: 'location',
              type: 'option',
              input: [],
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'upper pole',
                  value: 'upper pole'
                },
                {
                  id: uid(25),
                  label: 'mid pole',
                  value: 'mid pole'
                },
                {
                  id: uid(25),
                  label: 'lower pole',
                  value: 'lower pole'
                }]
            }
          ]
        },
        {
          id: uid(25),
          label: 'S.O.L',
          type: 'checkbox',
          input: false,
          children: [
            {
              id: uid(25),
              label: 'number',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'one',
                  value: 'one'
                },
                {
                  id: uid(25),
                  label: 'few',
                  value: 'few'
                },
                {
                  id: uid(25),
                  label: 'many',
                  value: 'many'
                },
              ]
            },
            {
              id: uid(25),
              label: 'size up to',
              type: 'text',
              input: ''
            },
            {
              id: uid(25),
              label: 'components',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'cystic',
                  value: 'cystic'
                },
                {
                  id: uid(25),
                  label: 'solid',
                  value: 'solid'
                },
                {
                  id: uid(25),
                  label: 'mixture',
                  value: 'mixture'
                }
              ]
            },
            {
              id: uid(25),
              label: 'echogenicity',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'hyperechoic',
                  value: 'hyperechoic'
                },
                {
                  id: uid(25),
                  label: 'isoechoic',
                  value: 'isoechoic'
                },
                {
                  id: uid(25),
                  label: 'hypoechoic',
                  value: 'hypoechoic'
                },
                {
                  id: uid(25),
                  label: 'anechoic',
                  value: 'anechoic'
                }
              ]
            },
            {
              id: uid(25),
              label: 'morphology',
              type: 'option',
              input: [],
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'lobular',
                  value: 'lobular'
                },
                {
                  id: uid(25),
                  label: 'loculated',
                  value: 'loculated'
                },
                {
                  id: uid(25),
                  label: 'encapsulated',
                  value: 'encapsulated'
                },
                {
                  id: uid(25),
                  label: 'septated',
                  value: 'septated'
                },
                {
                  id: uid(25),
                  label: 'internal calcification',
                  value: 'internal calcification'
                },
                {
                  id: uid(25),
                  label: 'fat component',
                  value: 'fat component'
                },
                {
                  id: uid(25),
                  label: 'hemorhage',
                  value: 'hemorhage'
                }
              ]
            },
            {
              id: uid(25),
              label: 'border',
              type: 'option',
              input: '',
              multiple: false,
              options: [
                {
                  id: uid(25),
                  label: 'regular',
                  value: 'regular'
                },
                {
                  id: uid(25),
                  label: 'irregular',
                  value: 'irregular'
                }
              ]
            },
            {
              id: uid(25),
              label: 'location',
              type: 'option',
              input: [],
              multiple: true,
              options: [
                {
                  id: uid(25),
                  label: 'upper pole',
                  value: 'upper pole'
                },
                {
                  id: uid(25),
                  label: 'mid pole',
                  value: 'mid pole'
                },
                {
                  id: uid(25),
                  label: 'lower pole',
                  value: 'lower pole'
                }
              ]
            },
            {
              id: uid(25),
              label: 'invasion to surrounding tissue',
              type: 'checkbox',
              input: false,
            }

          ]
        },
        {
          id: uid(25),
          label: 'Internal Debris, Possibility of Inflammatory Process, Correlation with Bun/Cr',
          type: 'checkbox',
          input: false
        },
        {
          id: uid(25),
          label: 'in favor of',
          type: 'text',
          input: ''
        }
      ]
    }
  ]
}

const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value

    }
    state.inputFields = inputFields
  }
}

const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly

    // The node has not been found and we have no more options
    return null
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}
const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
