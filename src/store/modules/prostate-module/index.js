import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'Volume',
      input: '',
      type: 'text',
      colValue: '6'
    },
    {
      id: uid(25),
      label: 'size',
      type: 'option',
      multiple: false,
      input: 'normal',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text'
          }]
        },
        {
          id: uid(25),
          label: 'small',
          value: 'small',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text'
          }]
        },
        {
          id: uid(25),
          label: 'prominent(enlarged)',
          value: 'enlarged',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text'
          },
            {
              id: uid(25),
              label: 'type',
              type: 'option',
              multiple: false,
              input: '',
              options: [{
                id: uid(25),
                label: 'Symmetrically',
                value: 'symmetrically'
              },
                {
                  id: uid(25),
                  label: 'Non Symmetric',
                  value: 'non symmetric'
                },]
            },
            {
              id: uid(25),
              label: 'Suggestive Of BPH',
              type: 'checkbox',
              input: false,
            },]
        }
      ]
    },
    {
      id: uid(25),
      label: 'Shape',
      type: 'option',
      multiple: false,
      input: 'normal',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal'
        },
        {
          id: uid(25),
          label: 'Abnormal',
          value: 'abnormal',
        }]
    },
    {
      id: uid(25),
      label: 'Capsula',
      type: 'option',
      multiple: false,
      input: 'intact',
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'Intact',
          value: 'intact'
        },
        {
          id: uid(25),
          label: 'Not intact',
          value: 'not intact',

        }]
    },
    {
      id: uid(25),
      label: 'space of occupying lesion',
      type: 'checkbox',
      input: false,
      children: [
        {
          id: uid(25),
          label: 'Number',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'One',
              value: 'one'
            },
            {
              id: uid(25),
              label: 'Few',
              value: 'few'

            },
            {
              id: uid(25),
              label: 'Many',
              value: 'many'
            },
          ],
        },
        {
          id: uid(25),
          label: 'components',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'cyst',
              value: 'cyst'
            },
            {
              id: uid(25),
              label: 'solid',
              value: 'solid'
            },
            {
              id: uid(25),
              label: 'mixture',
              value: 'mixture'
            }

          ]
        },
        {
          id: uid(25),
          label: 'morphology',
          type: 'option',
          multiple: true,
          input: [],
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'tubular',
              value: 'tubular'
            },

            {
              id: uid(25),
              label: 'lobular',
              value: 'lobular'
            },

            {
              id: uid(25),
              label: 'loculated',
              value: 'loculated'
            },

            {
              id: uid(25),
              label: 'internal hemorrhage',
              value: 'internal hemorrhage'
            },

            {
              id: uid(25),
              label: 'internal calcification',
              value: 'internal calcification'
            },
            {
              id: uid(25),
              label: 'Separated',
              value: 'Separated'
            },
            {
              id: uid(25),
              label: 'internal vascularity',
              value: 'internal vascularity'
            }
          ]
        },

        {
          id: uid(25),
          label: 'Location',
          type: 'option',
          multiple: true,
          input: [],
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'peripheral zone',
              value: 'peripheral zone'
            },
            {
              id: uid(25),
              label: 'transitional zone',
              value: 'transitional zone'
            },
            {
              id: uid(25),
              label: 'central zone',
              value: 'central zone'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Echogenicity',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'Hypoechoic',
              value: 'hypoechoic'
            },

            {
              id: uid(25),
              label: 'Hyperechoic',
              value: 'hyperechoic'
            },

            {
              id: uid(25),
              label: 'Isoechoic',
              value: 'isoechoic'
            },
            {
              id: uid(25),
              label: 'Nonechoic',
              value: 'nonechoic'
            }
          ]
        },

        {
          id: uid(25),
          label: 'Border',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'Regular',
              value: 'regular'
            },
            {
              id: uid(25),
              label: 'Irregular',
              value: 'irregular'
            }
          ]
        },

        {
          id: uid(25),
          label: 'Internal echo',
          type: 'option',
          multiple: false,
          input: '',
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'homogeneous',
              value: 'homogeneous'
            },
            {
              id: uid(25),
              label: 'heterogeneous',
              value: 'heterogeneous'
            }
          ]
        },
        {
          id: uid(25),
          type: 'text',
          input: '',
          label: 'Size Up To',
        },
        {
          id: uid(25),
          type: 'checkbox',
          input: false,
          label: 'invasion to surrounding tissue/fibromuscular stroma',
        },

        {
          id: uid(25),
          type: 'checkbox',
          input: false,
          label: 'sign of inflammation/collection around lesion',
        },

        {
          id: uid(25),
          label: 'suggestive of:',
          type: 'option',
          multiple: true,
          input: [],
          options: [
            {
              id: uid(25),
              label: 'benign lesion',
              value: 'benign lesion'
            },
            {
              id: uid(25),
              label: 'malignant lesion/carcinoma',
              value: 'malignant lesion/carcinoma'
            }
          ]
        }]
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'sign of prostatitis',
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      label: 'correlation with PSA is indicated'
    }
  ],
  // inputFields: [
  //   {
  //     id: uid(25),
  //     label: 'Observation Status',
  //     type: 'option',
  //     input: 'observed',
  //     multiple: false,
  //     colValue: '12',
  //     options: [
  //       {
  //         id: uid(25),
  //         label: 'Not seen',
  //         value: 'not seen',
  //       },
  //       {
  //         id: uid(25),
  //         label: 'Observed',
  //         value: 'observed',
  //         children: [
  //           {
  //             id: uid(25),
  //             label: 'Volume',
  //             input: '',
  //             type: 'text'
  //           },
  //           {
  //             id: uid(25),
  //             label: 'size',
  //             type: 'option',
  //             multiple: false,
  //             input: 'normal',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'normal',
  //                 value: 'normal',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Measured Size',
  //                   input: '',
  //                   type: 'text'
  //                 }]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'small',
  //                 value: 'small',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Measured Size',
  //                   input: '',
  //                   type: 'text'
  //                 }]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'prominent(enlarged)',
  //                 value: 'enlarged',
  //                 children: [{
  //                   id: uid(25),
  //                   label: 'Measured Size',
  //                   input: '',
  //                   type: 'text'
  //                 },
  //                   {
  //                     id: uid(25),
  //                     label: 'type',
  //                     type: 'option',
  //                     multiple: false,
  //                     input: '',
  //                     options: [{
  //                       id: uid(25),
  //                       label: 'Symmetrically',
  //                       value: 'symmetrically'
  //                     },
  //                       {
  //                         id: uid(25),
  //                         label: 'Non Symmetric',
  //                         value: 'non symmetric'
  //                       },]
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'Suggestive Of BPH',
  //                     type: 'checkbox',
  //                     input: false,
  //                   },]
  //               }
  //             ]
  //           },
  //           {
  //             id: uid(25),
  //             label: 'Shape',
  //             type: 'option',
  //             multiple: false,
  //             input: 'normal',
  //             colValue: '6',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'normal',
  //                 value: 'normal'
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'Abnormal',
  //                 value: 'abnormal',
  //               }]
  //           },
  //           {
  //             id: uid(25),
  //             label: 'Capsula',
  //             type: 'option',
  //             multiple: false,
  //             input: 'intact',
  //             options: [
  //               {
  //                 id: uid(25),
  //                 label: 'Intact',
  //                 value: 'intact'
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'Not intact',
  //                 value: 'not intact',
  //
  //               }]
  //           },
  //           {
  //             id: uid(25),
  //             label: 'space of occupying lesion',
  //             type: 'checkbox',
  //             input: false,
  //             children: [
  //               {
  //                 id: uid(25),
  //                 label: 'Number',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'One',
  //                     value: 'one'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'Few',
  //                     value: 'few'
  //
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'Many',
  //                     value: 'many'
  //                   },
  //                 ],
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'components',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'cyst',
  //                     value: 'cyst'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'solid',
  //                     value: 'solid'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'mixture',
  //                     value: 'mixture'
  //                   }
  //
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'morphology',
  //                 type: 'option',
  //                 multiple: true,
  //                 input: [],
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'tubular',
  //                     value: 'tubular'
  //                   },
  //
  //                   {
  //                     id: uid(25),
  //                     label: 'lobular',
  //                     value: 'lobular'
  //                   },
  //
  //                   {
  //                     id: uid(25),
  //                     label: 'loculated',
  //                     value: 'loculated'
  //                   },
  //
  //                   {
  //                     id: uid(25),
  //                     label: 'internal hemorrhage',
  //                     value: 'internal hemorrhage'
  //                   },
  //
  //                   {
  //                     id: uid(25),
  //                     label: 'internal calcification',
  //                     value: 'internal calcification'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'Separated',
  //                     value: 'Separated'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'internal vascularity',
  //                     value: 'internal vascularity'
  //                   }
  //                 ]
  //               },
  //
  //               {
  //                 id: uid(25),
  //                 label: 'Location',
  //                 type: 'option',
  //                 multiple: true,
  //                 input: [],
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'peripheral zone',
  //                     value: 'peripheral zone'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'transitional zone',
  //                     value: 'transitional zone'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'central zone',
  //                     value: 'central zone'
  //                   }
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 label: 'Echogenicity',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'Hypoechoic',
  //                     value: 'hypoechoic'
  //                   },
  //
  //                   {
  //                     id: uid(25),
  //                     label: 'Hyperechoic',
  //                     value: 'hyperechoic'
  //                   },
  //
  //                   {
  //                     id: uid(25),
  //                     label: 'Isoechoic',
  //                     value: 'isoechoic'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'Nonechoic',
  //                     value: 'nonechoic'
  //                   }
  //                 ]
  //               },
  //
  //               {
  //                 id: uid(25),
  //                 label: 'Border',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'Regular',
  //                     value: 'regular'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'Irregular',
  //                     value: 'irregular'
  //                   }
  //                 ]
  //               },
  //
  //               {
  //                 id: uid(25),
  //                 label: 'Internal echo',
  //                 type: 'option',
  //                 multiple: false,
  //                 input: '',
  //                 colValue: '6',
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'homogeneous',
  //                     value: 'homogeneous'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'heterogeneous',
  //                     value: 'heterogeneous'
  //                   }
  //                 ]
  //               },
  //               {
  //                 id: uid(25),
  //                 type: 'text',
  //                 input: '',
  //                 label: 'Size Up To',
  //               },
  //               {
  //                 id: uid(25),
  //                 type: 'checkbox',
  //                 input: false,
  //                 label: 'invasion to surrounding tissue/fibromuscular stroma',
  //               },
  //
  //               {
  //                 id: uid(25),
  //                 type: 'checkbox',
  //                 input: false,
  //                 label: 'sign of inflammation/collection around lesion',
  //               },
  //
  //               {
  //                 id: uid(25),
  //                 label: 'suggestive of:',
  //                 type: 'option',
  //                 multiple: true,
  //                 input: [],
  //                 options: [
  //                   {
  //                     id: uid(25),
  //                     label: 'benign lesion',
  //                     value: 'benign lesion'
  //                   },
  //                   {
  //                     id: uid(25),
  //                     label: 'malignant lesion/carcinoma',
  //                     value: 'malignant lesion/carcinoma'
  //                   }
  //                 ]
  //               }]
  //           },
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'sign of prostatitis',
  //           },
  //           {
  //             id: uid(25),
  //             type: 'checkbox',
  //             input: false,
  //             label: 'correlation with PSA is indicated'
  //           }
  //         ]
  //       },
  //     ]
  //   },
  // ]
}

const mutations = {
  updateFieldValue(state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    });
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value;
    } else {
      inputItem.input = payload.value;

    }
    state.inputFields = inputFields;
  }
}

const actions = {
  updateLiverValue(context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode(context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions(context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result;
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j]);
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise 'return' will not work properly

    // The node has not been found and we have no more options
    return null;
  }
}

const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if('multiple' in node && node.multiple && val.input.includes(node.options[i])){
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
      }
      else if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}

const getters = {
  inputFields: (state) => {
    return state.inputFields
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
