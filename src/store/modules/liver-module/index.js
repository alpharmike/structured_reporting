import Vue from 'vue'
import uid from 'uid'

const state = {
  completed: false,
  inputFields: [
    {
      id: uid(25),
      label: 'size',
      type: 'option',
      input: 'normal',
      multiple: false,
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text',
            colValue: '12',
          }]
        },
        {
          id: uid(25),
          label: 'small',
          value: 'small',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text',
            colValue: '12',
          }]
        },
        {
          id: uid(25),
          label: 'enlarged',
          value: 'enlarged',
          children: [{
            id: uid(25),
            label: 'Measured Size',
            input: '',
            type: 'text',
            colValue: '12',
          }
          ]
        }
      ]
    },
    {
      id: uid(25),
      label: 'parenchymal echogenicity ',
      type: 'option',
      input: 'normal',
      multiple: false,
      colValue: '6',
      options: [
        {
          id: uid(25),
          label: 'normal',
          value: 'normal'
        },
        {
          id: uid(25),
          label: 'heterogeneous',
          value: 'heterogeneous'
        },
        {
          id: uid(25),
          label: 'increased',
          value: 'increased'
        },
        {
          id: uid(25),
          label: 'suggestive of fatty liver',
          value: 'suggestive of fatty liver'
        },
        {
          id: uid(25),
          label: 'suggestive of cirrhotic change',
          value: 'suggestive of cirrhotic change'
        }

      ]
    },
    {
      id: uid(25),
      label: 'sign of space occupying lesion ',
      type: 'checkbox',
      input: false,
      colValue: '6',
      children: [
        {
          id: uid(25),
          label: 'Number',
          input: '',
          type: 'option',
          multiple: false,
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'one',
              value: 'one'
            },
            {
              id: uid(25),
              label: 'few',
              value: 'few'

            },
            {
              id: uid(25),
              label: 'many',
              value: 'many'
            }
          ]
        },
        {
          id: uid(25),
          label: 'Echogenicity',
          input: '',
          type: 'option',
          multiple: false,
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'hypoecho',
              value: 'hypoecho'
            },
            {
              id: uid(25),
              label: 'hyperecho',
              value: 'hyperecho'
            },
            {
              id: uid(25),
              label: 'inhomogeneous',
              value: 'inhomogeneous'
            }

          ]
        },
        {
          id: uid(25),
          label: 'components',
          input: '',
          type: 'option',
          multiple: false,
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'cystic',
              value: 'cystic'
            },
            {
              id: uid(25),
              label: 'solid',
              value: 'solid'
            },
            {
              id: uid(25),
              label: 'mixture',
              value: 'mixture'
            }
          ]
        },
        {
          id: uid(25),
          type: 'option',
          input: [],
          label: 'Position',
          multiple: true,
          colValue: '6',
          options: [
            {
              id: uid(25),
              label: 'right lateral lobe',
              value: 'right lateral lobe'
            },
            {
              id: uid(25),
              label: 'right medial lobe',
              value: 'right medial lobe'
            },
            {
              id: uid(25),
              label: 'left ant lobe',
              value: 'left ant lobe'
            },
            {
              id: uid(25),
              label: 'left pos lobe',
              value: 'left pos lobe'
            },
            {
              id: uid(25),
              label: 'caudata lobe',
              value: 'caudata lobe'
            },
          ]
        },
        {
          id: uid(25),
          type: 'text',
          input: '',
          label: 'size',
          colValue: '6',
        },
        {
          id: uid(25),
          type: 'text',
          input: '',
          label: 'Suggestive of',
          colValue: '6',
        },
        {
          id: uid(25),
          type: 'text',
          input: '',
          label: 'Any comment',
          colValue: '12',
        },
      ]
    },
    {
      id: uid(25),
      type: 'checkbox',
      input: false,
      colValue: '6',
      label: 'intrahepatic bile duct dilatation',
    },

  ]
}

const mutations = {
  updateFieldValue (state, payload) {
    let inputItem = null
    let inputFields = [...state.inputFields]
    inputFields.forEach((inputField) => {
      let res = recursiveCall(payload.id, inputField)
      if (res !== null) {
        inputItem = res
      }
    })
    if (inputItem.type === 'option' || inputItem.type === 'checkbox') {
      inputItem.input = payload.value
    } else {
      inputItem.input = payload.value

    }
    state.inputFields = inputFields
  },

  toggleCompleted (state) {
    state.completed = true
  }
}

const actions = {
  updateLiverValue (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  findNode (context, payload) {
    context.commit('updateFieldValue', payload)
  },

  checkForMoreOptions (context, field) {
    let result
    context.state.inputFields.forEach((inputField) => {
      if (field.label === inputField.label) {
        inputField.options.forEach((option) => {
          if (option.value === field.value) {
            result = option.options
          }
        })
      }
    })
    return result
  },

  toggleCompleted (context) {
    context.commit('toggleCompleted')
  }
}

let recursiveCall = (id, currentNode) => {
  let i,
    currentChild,
    result
  if (id === currentNode.id) {
    return currentNode
  } else {
    if (currentNode.type === 'checkbox') {
      for (let j = 0; currentNode.children && j < currentNode.children.length; ++j) {
        result = recursiveCall(id, currentNode.children[j])
        // Return the result if the node has been found
        if (result !== null) {
          return result
        }
      }

    } else if (currentNode.type === 'option') {
      for (i = 0; currentNode.options && i < currentNode.options.length; i += 1) {
        let j
        for (j = 0; currentNode.options[i].children && j < currentNode.options[i].children.length; ++j) {
          result = recursiveCall(id, currentNode.options[i].children[j])
          // Return the result if the node has been found
          if (result !== null) {
            return result
          }
        }
      }
    }

    // Use a for loop instead of forEach to avoid nested functions
    // Otherwise "return" will not work properly

    // The node has not been found and we have no more options
    return null
  }
}
const findValue = (node) => {
  let val = {
    label: node.label,
    input: node.input,
    id: node.id,
    type: node.type,
  }
  if (val.type === 'option') {
    val.children = []
    for (let i = 0; i < node.options.length; ++i) {
      if (node.options[i].value === val.input) {
        if ('children' in node.options[i]) {
          for (let j = 0; j < node.options[i].children.length; ++j) {
            val.children.push(findValue(node.options[i].children[j]))
          }
        }
        break;
      }
    }
  } else if ('children' in node && val.type === 'checkbox') {
    val.children = []
    for (let i = 0; i < node.children.length; ++i) {
      val.children.push(findValue(node.children[i]))
    }
  }
  return val
}
const getters = {
  inputFields: (state) => {
    return state.inputFields
  },

  completed: (state) => {
    return state.completed
  },
  formValue: (state) => {
    let values = []
    for (let i = 0; i < state.inputFields.length; ++i) {
      values.push(findValue(state.inputFields[i]))
    }
    return values
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}





































