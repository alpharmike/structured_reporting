import Vue from 'vue'
import Vuex from 'vuex'
import patient from "./modules/patient-module"
import kidney from "./modules/kidney-module"
import liver from "./modules/liver-module"
import prostate from "./modules/prostate-module"
import pancreas from "./modules/pancreas-module"
import gallBladder from "./modules/gall-bladder-module"
import cbd from "./modules/CBD-module"
import vasculature from "./modules/vasculature-module"
import spleen from "./modules/spleen-module"
import uterus from "./modules/uterus-module"
import ovaries from "./modules/ovaries-module"
import seminalVesicle from "./modules/seminal-vesicle-module"
import urinaryTract from "./modules/urinary-tract-module"
import abdominoPelvicCavity from "./modules/AbdominopelvicCavity"
import urinaryBladder from "./modules/urinary-bladder-module"
import ureter from "./modules/ureter-module"

// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
      patient,
      kidney,
      prostate,
      gallBladder,
      cbd,
      vasculature,
      spleen,
      uterus,
      ovaries,
      pancreas,
      urinaryTract,
      seminalVesicle,
      liver,
      abdominoPelvicCavity,
      urinaryBladder,
      ureter
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
