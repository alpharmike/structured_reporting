// import something here
import Vue from 'vue'
import VuePageTransition from 'vue-page-transition'

Vue.use(VuePageTransition);

// "async" is optional
export default ({ /* app, router, Vue, ... */ }) => {
  // something to do
}
