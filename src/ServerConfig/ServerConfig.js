const host = "https://structured-report-backend.herokuapp.com";

let urls = {
  CreatePatient: host + '/patient/create',
  GetAllPatients: host + '/patient/all',
  GetPatientReports: host + '/patient',
  GetReport: host + '/report',
  CreateReport: host + '/report/create',
}

export default urls;

